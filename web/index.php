<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 17/04/2019
 * Time: 11:27
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php' ;

//config pour les erreurs
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);

//création de l'application avec affichage des erreurs
$app = new \Slim\App($c);

//base de donnés
$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file("src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

//on récupère le container
$container = $app->getContainer();

//on créé le rendeur de vue
$container["view"] = new Slim\Views\PhpRenderer("./");

//on démarre la session
session_start();


//en dessous on met les routes
//pour le jeu en lui même projet/main.php


$app->get("/projet/{id}/{token}", function (Request $request, Response $response, $args){
    $q = \test\modele\Questionnaire::find($args["id"]);
    if($q->token == $args["token"]){
        $_SESSION["afficherReprésentation"] = $args["id"];
        return $this->view->render($response, 'src/jeu/phaser/projet/main.php');
    }else{
        echo "vous n'avez pas accès à cette page";
    }

})->setName("projet");


$app->get("/demarrer", function (Request $request, Response $response){
    return $this->view->render($response, 'src/html/questionnaire.php');
});

$app->get("/questionnaire", function (Request$request, Response $response){
    return $this->view->render($response, 'src/html/section.php');
});

$app->get("/questionnaire/{id}", function (Request $request, Response $response, $args){
    $controleur = new \test\controleur\QuestionnaireControleur();
    $controleur->afficherConnexionQuestionnaire($this, $response, $args["id"]);
})->setName("afficherQuestionnaire");

$app->post("/questionnaire/{id}", function (Request $request, Response $response, $args){
    $controleur = new \test\controleur\QuestionnaireControleur();
    return $controleur->traiterConnexion($this, $response, $args, $this->view);
});

$app->get("/river/inscription", function ($request, $reponse, $args){
    $controleur = new \test\controleur\CompteControleur();
    $controleur->afficherInscription($this, $reponse);
})->setName("inscriptionRiver");

$app->post("/river/inscription", function ($request, $reponse, $args){
    $controleur = new \test\controleur\CompteControleur();
    return $controleur->traiterInscription($this, $reponse);
});

$app->get("/river/connexion", function ($request, $reponse){
    $controleur = new \test\controleur\CompteControleur();
    return $controleur->afficherConnexion($this, $reponse);
})->setName("connexionRiver");

$app->post("/river/connexion", function ($request, $reponse){
    $controleur = new \test\controleur\CompteControleur();
    return $controleur->traiterConnexion($this, $reponse);
});

$app->get("/river/afficherMenu", function ($request, $reponse){
    $controleur = new \test\controleur\CompteControleur();
    return $controleur->afficherMenu($this, $reponse);
})->setName("menuRiver");

$app->get("/river/nouveauQuestionnaire", function ($request, $reponse){
    $controleur = new  \test\controleur\QuestionnaireCreationControleur();
    $controleur->nouveauQuestionnaire($this, $reponse);
})->setName("nouveauQuestionnaire");

$app->post("/river/nouveauQuestionnaire", function ($request, $reponse){
    $controleur = new \test\controleur\QuestionnaireCreationControleur();
    return $controleur->traiterNouveauQuestionnaire($this, $reponse);
});

$app->get("/river/questionnaire/{nom}/{id}", function ($request, $reponse, $args){
    $controleur = new \test\controleur\QuestionnaireCreationControleur();
    return $controleur->creationQuestionnaire($this, $reponse, $args);
})->setName("creationQuestionnaire");

$app->post("/river/questionnaire/{nom}/{id}", function ($request, $reponse, $args){
    $controleur = new \test\controleur\QuestionnaireCreationControleur();
    return $controleur->suppressionQuestionnaire($this, $reponse, $args);
})->setName("suppressionQuestionnaire");

$app->get("/river/afficherAnciensQuestionnaires", function ($req, $rep, $args){
    $controleur = new \test\controleur\QuestionnaireCreationControleur();
    $controleur->anciensQuestionnaires($this, $rep, $args);
})->setName("afficherAnciensQuestionnaires");

$app->get("/river/creation/section/{idSection}", function ($req, $rep, $args){
    $controleur = new \test\controleur\SectionControleur();
    $controleur->modificationSection($this, $rep, $args);
})->setName("sectionCreation");

$app->get("/river/deconnexion", function ($req, $rep, $args){
    $controleur = new \test\controleur\CompteControleur();
    return $controleur->deconnexion($this, $rep);
})->setName("deconnexion");

$app->get("/river/getRepresentationVisuelle", function (){
    if(isset($_SESSION["afficherReprésentation"])){
        echo $_SESSION["afficherReprésentation"];
    }
});












//api création questionnaire
$app->post("/api/creation/nouvelleSection", function ($req, $rep, $args){
    $controleur = new \test\controleur\SectionControleur();
    $controleur->enregistrerNouvelleSection($this, $rep, $args);
});

$app->delete("/api/supprimer/section/{numSection}", function ($req, $rep,$args){
    $controleur = new \test\controleur\SectionControleur();
    $controleur->supprimerSection($args);
});

$app->post("/api/creation/nouvelElement", function ($req, $rep, $args){
    $controleur = new \test\controleur\ElementControleur();
    $controleur->ajouterNouvelElement();
});

$app->delete("/api/supprimer/element/{numElement}", function ($req, $rep,$args){
    $controleur = new \test\controleur\ElementControleur();
    $controleur->supprimerElement($args);
});

$app->post("/api/creation/infoContact", function ($req, $rep, $args){
    $controleur = new \test\controleur\QuestionnaireCreationControleur();
    $controleur->traiterInfosContact($this, $rep, $args);
});

$app->post("/api/creation/nouvelleQuestion", function ($req, $rep, $args){
    $controleur = new \test\controleur\QuestionControleur();
    $controleur->insererNouvelleQuestion($this, $rep, $args);
});

$app->delete("/api/supprimer/question/{idQuestion}", function ($req, $rep,$args){
    $controleur = new \test\controleur\QuestionControleur();
    $controleur->supprimerQuestion($this, $rep, $args);
});

$app->get("/api/sections/get", function ($req, $rep, $args){
    $controleur = new \test\controleur\SectionControleur();
    return $controleur->getSections($this, $rep, $args, null);
});

$app->get("/api/sections/get/{debut}", function ($req, $rep, $args){
    $controleur = new \test\controleur\SectionControleur();
    return $controleur->getSections($this, $rep, $args, $args["debut"]);
});

$app->get("/api/elementsComparaison/get", function ($req, $rep, $args){
    $controleur = new \test\controleur\ElementControleur();
    return $controleur->getElements($this, $rep, $args, null);
});

$app->get("/api/elementsComparaison/get/{debut}", function ($req, $rep, $args){
    $controleur = new \test\controleur\ElementControleur();
    return $controleur->getElements($this, $rep, $args, $args["debut"]);
});

$app->get("/api/questions/get", function ($req, $rep, $args){
    $controleur = new \test\controleur\QuestionControleur();
    return $controleur->getQuestions($this, $rep, $args, null);
});

$app->get("/api/questions/get/{debut}", function ($req, $rep, $args){
    $controleur = new \test\controleur\QuestionControleur();
    return $controleur->getQuestions($this, $rep, $args, $args["debut"]);
});

$app->get("/api/representation/{id}", function($req, $rep, $args){
    $controleur = new \test\controleur\QuestionnaireControleur();
    return $controleur->getRepresentation($this, $rep, $args);
});

$app->get("/api/graphiques", function($req, $rep, $args){
    $controleur = new \test\controleur\QuestionnaireControleur();
    return $controleur->getGraphiques($this, $rep, $args);
});

$app->get("/api/csv/{num}", function ($req, $rep, $args){
    $controleur = new \test\controleur\QuestionnaireControleur();
    $controleur->getCSV($args);
})->setName("csv");















//Voici les routes pour l'api
//pour afficher un questionnaire et ses sections
$app->get("/api/questionnaire/{id}", function ($request, $response, $args){
    $c = new \test\controleur\QuestionnaireControleur();
    //on retourne soit l'erreur soit le html
    return $c->questionnaireEtSections($args["id"], $this, $response);
})->setName("questionnaire");

//pour l'adresse du questionnaire actif
$app->get("/api/questionnaireActif", function ($request, $response, $args){
    $c = new \test\controleur\QuestionnaireControleur();
    //on retourne soit l'erreur soit le html
    return $c->questionnaireActif($this, $response);
});

//pour avoir la section profession
$app->get("/api/sectionProfession", function ($request, $response, $args){
    $c = new \test\controleur\SectionControleur();
    //on retourne soit l'erreur soit le html
    return $c->fabriquerSectionProfession($this, $response);
})->setName("sectionProfession");

//pour avoir les professions
$app->get("/api/professions", function ($request, $response, $args){
    $c = new \test\controleur\SectionControleur();
    //on retourne soit l'erreur soit le html
    return $c->professions($this, $response);
})->setName("professions");

//pour avoir les spécialités
$app->get("/api/specialites", function ($request, $response, $args){
    $c = new \test\controleur\SectionControleur();
    //on retourne soit l'erreur soit le html
    return $c->specialites($this, $response);
})->setName("specialites");











//pour afficher une section détaillée
$app->get("/api/questionnaire/{idQuestionnaire}/section/{idSection}", function ($request, $response, $args){
    $c = new \test\controleur\SectionControleur();
    return $c->sectionEtQuestions($args, $this, $response);
})->setName("sectionDetaillee");





//affichage d'une question détaillée
$app->get("/api/question/{id}", function ($request, $response, $args){
    $c = new \test\controleur\QuestionControleur();
    return $c->questionDetaillee($args, $this, $response);
})->setName("questionDetaille");

//renvoi le questionnaire qui était voulu
$app->get("/api/questionnaireVoulu", function ($request, $response, $args){
    $controleur = new \test\controleur\QuestionnaireControleur();
    return $controleur->questionnaireVoulu($this, $response);
})->setName("questionnaireVoulu");

//sauvegarde d'une question de comparaison
$app->post("/api/save/question/comparaison", function ($request, $reponse, $args){
    $controleur = new \test\controleur\InsertionControleur();
    return $controleur->insererComparaison($this, $reponse);
});

//sauvegarde de la profession
$app->post("/api/save/question/profession", function ($request, $reponse, $args){
    $controleur = new \test\controleur\InsertionControleur();
    return $controleur->insererProfession($this, $reponse);
});

//sauvegarde de la specialité
$app->post("/api/save/question/specialite", function ($request, $reponse, $args){
    $controleur = new \test\controleur\InsertionControleur();
    return $controleur->insererSpecialite($this, $reponse);
});

//sauvegarde de la resultat
$app->post("/api/save/question/resultat", function ($request, $reponse, $args){
    $controleur = new \test\controleur\InsertionControleur();
    return $controleur->insererResultat($this, $reponse);
});

//sauvegarde d'entretiens
$app->post("/api/save/question/entretien", function ($request, $reponse, $args){
    $controleur = new \test\controleur\InsertionControleur();
    return $controleur->insererEntretien($this, $reponse);
});

//sauvegarde des questions de contact
$app->post("/api/save/question/contact", function ($request, $reponse, $args){
    $controleur = new \test\controleur\InsertionControleur();
    return $controleur->insererContact($this, $reponse);
});

//sauvegarde du mail
$app->post("/api/save/question/mail", function ($request, $reponse, $args){
    $controleur = new \test\controleur\InsertionControleur();
    return $controleur->insererMail($this, $reponse);
});

//on récupère l'id de la personne
$app->get("/api/personne/id", function ($request, $reponse, $args){
    $controleur = new \test\controleur\PersonneControleur();
    return $controleur->getIdPersonne($this, $reponse);
});








$app->delete("/test", function (){
    var_dump($_REQUEST);
});

$app->get("/test", function (){

});





$app->run();