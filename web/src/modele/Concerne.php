<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 23/04/2019
 * Time: 11:15
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Concerne extends Model {
    protected $table = "Concerne";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function reponse(){
        return $this->belongsTo("test\modele\Reponse", "idReponse");
    }

    public function methode(){
        return $this->belongsTo("js\modele\Methode", "idMethode");
    }

    public function reponsePossible(){
        return $this->belongsTo("js\modele\ReponsePossible", "idReponsePossible");
    }
}