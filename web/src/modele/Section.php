<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:15
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Section extends Model {
    protected $table = "Section";
    protected $primaryKey = "idSection";
    public $timestamps = false;

    public function questions(){
        return $this->hasMany("test\modele\Question", "idSection");
    }

    public function questionnaires(){
        return $this->belongsToMany("test\modele\Questionnaire", "APourSection", "idSection", "idQuestionnaire");
    }
}