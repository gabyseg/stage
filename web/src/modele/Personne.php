<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:28
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Personne extends Model {
    protected $table = "Personne";
    protected $primaryKey = "idPersonne";
    public $timestamps = false;


    public function reponses(){
        return $this->hasMany("test\modele\Reponse", "idPersonne");
    }

    public function contact(){
        return $this->belongsTo("test\modele\Contact", "idContact");
    }

    public function profession(){
        return $this->belongsTo("test\modele\Profession", "idProfession");
    }

    public function specialite(){
        return $this->belongsTo("test\Model\Specialite", "idSpecialite");
    }
}