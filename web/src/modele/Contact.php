<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:26
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Contact extends Model {
    protected $table = "Contact";
    protected $primaryKey = "idContact";
    public $timestamps = false;
}