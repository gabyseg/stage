<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:12
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model {
    protected $table = "Questionnaire";
    protected $primaryKey = "idQuestionnaire";
    public $timestamps = false;

    public function reponses(){
        return $this->hasMany("test\modele\Reponse", "idQuestionnaire");
    }

    public function questions(){
        return $this->belongsToMany("test\modele\Question", "Possede", "idQuestionnaire", "idQuestion");
    }

    public function sections(){
        return $this->belongsToMany("test\modele\Section","APourSection", "idQuestionnaire", "idSection");
    }

    public function elementsCompares(){
        return $this->belongsToMany("test\modele\Methode", "estCompareDans", "idQuestionnaire", "idElementComp");
    }
}