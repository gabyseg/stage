<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:24
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Methode extends Model {
    protected $table = "Methode";
    protected $primaryKey = "idMethode";
    public $timestamps = false;

    public function concerne(){
        return $this->hasMany("test\modele\Concerne", "idMethode");
    }

    public function estCompareDans(){
        return $this->belongsToMany("test\modele\Questionnaire", "estCompareDans", "idElementComp", "idQuestionnaire");
    }
}