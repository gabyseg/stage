<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:30
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Profession extends Model {
    protected $table = "Profession";
    protected $primaryKey = "idProfession";
    public $timestamps = false;
}