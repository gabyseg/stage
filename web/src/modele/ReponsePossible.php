<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:18
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class ReponsePossible extends Model {
    protected $table = "ReponsePossible";
    protected $primaryKey = "idReponse";
    public $timestamps = false;

    public function concerne(){
        return $this->hasMany("test\modele\Concerne", "idReponsePossible");
    }
}