<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:25
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Reponse extends Model{
    protected $table = "Reponse";
    protected $primaryKey = "idReponse";
    public $timestamps = false;

    public function questionnaire(){
        return $this->belongsTo("test\modele\Questionnaire", "idQuestionnaire");
    }

    public function question(){
        return $this->belongsTo("test\modele\Question", "idQuestion");
    }

    public function personne(){
        return $this->belongsTo("test\modele\Personne","idPersonne");
    }

    public function concerne(){
        return $this->hasMany("test\modele\Concerne", "idReponse");
    }
}