<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 06/06/2019
 * Time: 09:22
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Variable extends Model {
    protected $primaryKey = "id";
    protected $table = "Variable";
    public $timestamps = false;
}