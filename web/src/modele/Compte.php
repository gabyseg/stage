<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 20/05/2019
 * Time: 17:22
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Compte extends Model {
    protected $table = "Compte";
    protected $primaryKey = "idCompte";
    public $timestamps = false;
}