<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:16
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Question extends Model {
    protected $table = "Question";
    protected $primaryKey = "idQuestion";
    public $timestamps = false;

    public function section(){
        return $this->belongsTo("test\modele\Section", "idSection");
    }

    public function reponses(){
        return $this->hasMany("test\modele\Reponse", "idQuestion");
    }

    public function questionnaires(){
        return $this->belongsToMany("test\modele\Questionnaire", "Possede", "idQuestion", "idQuestionnaire");
    }

    public function reponsesPossibles(){
        return $this->hasMany("test\modele\ReponsePossible",  "idQuestion");
    }

    public function variable(){
        return $this->belongsTo("test\modele\Variable", "idVariable");
    }
}