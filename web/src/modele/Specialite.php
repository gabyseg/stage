<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 18/04/2019
 * Time: 13:32
 */

namespace test\modele;


use Illuminate\Database\Eloquent\Model;

class Specialite extends Model {
    protected $table = "Specialite";
    protected $primaryKey = "idSpecialite";
    public $timestamps = false;
}