<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 21/05/2019
 * Time: 15:02
 */

namespace test\vue;



class QuestionnaireVue{
    private $objets;

    public function __construct($donnees){
        $this->objets = $donnees;
    }

    private function nouveauQuestionnaireBase(){
        //on fabrique le titre
        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p class="titre"> Création d'un nouveau questionnaire</p>
        </div>
        <div class="row" id="informationsQuestionnaire">
            <div class="col-md-12">
                <form method="post">
                    <div class="form-group">
                        <label for="nom">Nom du questionnaire</label>
                        <input class="form-control" type="text" placeholder="Nom" name="nom" required>
                    </div>
                    <div class="form-group">
                        <label for="motDePasse1">Mot de passe pour accéder au questionnaire</label>
                        <input type="password" class="form-control" id="motDePasse1" placeholder="Mot de passe" name="motDePasse" required>
                    </div>
                    <div class="form-group">
                        <label for="motDePasse2">Répétez</label>
                        <input type="password" class="form-control" id="motDePasse2" placeholder="Répétez le mot de passe" required>
                    </div>
                    <button class="btn btn-danger" type="submit" name="informationsDeBase" value="valider">Enregistrer</button>
                    <p id="messageErreur" style='display: none'>test<span class="oi oi-circle-x"></span></p>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="../src/js/verifierConnexion/verifierConnexion.js"></script>
        
END;

        return $html;
    }

    private function nouveauQuestionnaire(){
        $routeur = $this->objets["routeur"];
        $route = $routeur->pathFor("menuRiver");

        //on récupère le titre du questionnaire
        $titre = $this->objets["nom"];

        //sections et éléments
        $s = $this->objets["sections"];
        $e = $this->objets["elements"];

        //questionnaire
        $q = $this->objets["questionnaire"];

        //pour l'adresse du questionnaire
        $adresse = $routeur->pathFor("afficherQuestionnaire", ["id"=>$q->idQuestionnaire]);
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $adresse;

        //pour l'adresse de river
        $url = $routeur->pathFor("projet", ["id" => $q->idQuestionnaire, "token" => $q->token]);
        $lienRiver = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . $url;

        //pour les boutons checked
        $entretienOui = "";
        $entretienNon = "";
        $resOui = "";
        $resNon = "";

        if($q->resultats == 1){
            $entretienOui = "checked";
        }else{
            $entretienNon = "checked";
        }
        if($q->entretien == 1){
            $resOui = "checked";
        }else{
            $resNon = "checked";
        }

        //on construit toutes les sections
        $sections = "";
        foreach ($s as $section){
            $nom = $section->intitule;
            $id = $section->idSection;
            $url = $routeur->pathFor("sectionCreation", ["idSection" => $id]);
            $res = <<<END
            <li id="s$id"><a href="$url">$nom <span class="oi oi-pencil boutonModif"></span></a> <span class="oi oi-delete float-right"></span></li>
END;
            $sections .= $res;
        }

        //on construit tous les éléments de comparaison
        $elements = "";
        foreach ($e as $element){
            $nom = $element->nomMethode;
            $id = $element->idMethode;
            $res = <<<END
            <li id="e$id"> $nom <span class="oi oi-delete boutonSupression"></span></li>
END;
            $elements .= $res;
        }

        //on construit la liste des personnes
        $personnes = "";
        foreach ($this->objets["contacts"] as $p){
            $nom = $p->nom;
            $prenom = $p->prenom;
            $mail = $p->mail;

            $res = <<<END
            <div class="row">
                <div class="col-md-3">$nom</div>
                <div class="col-md-3">$prenom</div>
                <div class="col-md-6">$mail</div>
            </div>
END;
            $personnes .= $res;
        }


        //pour le csv
        $routeCsv = $routeur->pathFor("csv", ["num"=>$q->idQuestionnaire]);

        //on fabrique le titre
        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p class="titre"> Création d'un nouveau questionnaire</p>
        </div>
        <div class="row" id="informationsQuestionnaire">
            <div class="col-md-12">
                <h4 class="">$titre 
                    <span class="oi oi-share-boxed pl-3" data-toggle="modal" data-target="#partage"></span> <form class="float-right" method="post"><button class="btn btn-danger" type="submit" name="supprimer">Supprimer le questionnaire</button></form>
                </h4>
            </div>
        </div>
        <div id="sectionsActuelles">
            <div class="row bordureTitre mt-2 mb-2">
                <p class="titre"> Les sections actuelles</p>
            </div>
            <ul id="sections">
                $sections
            </ul>
            <div id="boutonAjoutSection" data-toggle="modal" data-target="#ajoutSection">
                <p>Ajouter une section <span class="oi oi-plus"></span></p>
            </div>
        </div>
        <div id="elementsDeComparaison">
            <div class="row bordureTitre mt-2 mb-2">
                <p class="titre"> Les éléments de comparaison</p>
            </div>
            <ul id="elements">
                $elements
            </ul>
            <div id="boutonAjoutComparaison" data-toggle="modal" data-target="#ajoutElement">
                <p>Ajouter un élément de comparaison <span class="oi oi-plus"></span></p>
            </div>
        </div>
        <div id="Contact">
            <div class="row bordureTitre mt-2 mb-2">
                <p class="titre"> Contact</p>
            </div>
            <div>
                <div>
                    <h5>Voulez vous leur demander s'ils veulent avoir les résultats ?</h5>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="resultats" id="resultatsOui" value="oui" $resOui>
                    <label class="form-check-label" for="resultatsOui">
                        Oui
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="resultats" id="resultatsNon" value="non" $resNon>
                    <label class="form-check-label" for="resultatsNon">
                        Non
                    </label>
                </div>
                
                <div class="pt-3">
                    <h5>Voulez vous leur demander s'ils veulent avoir un entretien ?</h5>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="entretien" id="entretienOui" value="oui" $entretienOui>
                    <label class="form-check-label" for="entretienOui">
                        Oui
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="entretien" id="entretienNon" value="non" $entretienNon>
                    <label class="form-check-label" for="entretienNon">
                        Non
                    </label>
                </div>
                
                <button class="btn btn-danger mt-3 mb-3" data-toggle="modal" data-target="#listeContacts">Afficher la liste des personnes voulant être contactés</button>
                
            </div>
        </div>  
        <a class="btn btn-danger" href="$routeCsv">
            Récuper les données au format CSV
        </a>
        <p>
	        <a href="$route" id="retourEnArriere"><span class="oi oi-action-undo"></span>  Retour en arrière</a>
        </p>
END;

        $html .= <<<END
<!-- Fenêtre modale -->
        <div class="modal fade" id="ajoutSection" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog-centered modal-dialog modal-lg" role="document">
                <div class="modal-content bordureRouge">
                
                    <div class="modal-header">
                        <div class="text-center col-md">
                            <h4 class="modal-title">Ajouter une section</h4>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="section" id="nouvSection" value="nouvSection" checked>
                            <label class="form-check-label" for="nouvSection">
                                Nouvelle section
                            </label>
                        </div>
                        <div class="form-check pb-3">
                            <input class="form-check-input" type="radio" name="section" id="reuSection" value="reuSection">
                            <label class="form-check-label" for="reuSection">
                               Réutiliser une section
                            </label>
                        </div>
                    
                        <div class="form-group" id="sectio">
                            <label for="nouvelleSection">Nouvelle section</label>
                            <input type="text" class="form-control" id="nouvelleSection" placeholder="Nom de la nouvelle section">
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Annuler </button>
                        <button id="ajouterSection" type="button" class="btn btn-success" data-dismiss="modal">Ajouter</button>
                    </div>
                
                </div>
            </div>
        </div>
        
        <!-- Fenêtre modale -->
        <div class="modal fade" id="ajoutElement" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog-centered modal-dialog modal-lg" role="document">
                <div class="modal-content bordureRouge">
                
                    <div class="modal-header">
                        <div class="text-center col-md">
                            <h4 class="modal-title">Ajouter un élément de comparaison</h4>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                    
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="element" id="nouvElement" value="nouvElement" checked>
                            <label class="form-check-label" for="nouvElement">
                                Nouvel élément
                            </label>
                        </div>
                        <div class="form-check pb-3">
                            <input class="form-check-input" type="radio" name="element" id="reuElement" value="reuElement">
                            <label class="form-check-label" for="reuElement">
                               Réutiliser un élément
                            </label>
                        </div>
                    
                        <div class="form-group" id="elem">
                            <label for="nouvelElement">Nouvel élément de comparaison</label>
                            <input type="text" class="form-control" id="nouvelElement" placeholder="Nom du nouvel élément">
                        </div>
                        
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Annuler </button>
                        <button id="ajouterElement" type="button" class="btn btn-success" data-dismiss="modal">Ajouter</button>
                    </div>
                
                </div>
            </div>
        </div>
        
        <!-- Fenêtre modale -->
        <div class="modal fade" id="partage" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog-centered modal-dialog" role="document">
                <div class="modal-content bordureRouge">
                
                    <div class="modal-header">
                        <div class="text-center col-md">
                            <h4 class="modal-title">Partager le questionnaire</h4>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="adresse">Adresse du questionnaire</label>
                            <input type="text" class="form-control" id="adresse" value="$actual_link" readonly>
                        </div>
                        <div class="form-group">
                            <label for="river">Adresse du de la représentation avec RIVER</label>
                            <input type="text" class="form-control" id="river" value="$lienRiver" readonly>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
        
        <!-- Fenêtre modale -->
        <div class="modal fade" id="listeContacts" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog-centered modal-dialog modal-lg" role="document">
                <div class="modal-content bordureRouge">
                
                    <div class="modal-header">
                        <div class="text-center col-md">
                            <h4 class="modal-title">Liste des personnes voulant être contactées</h4>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                        $personnes
                    </div>
                </div>
            </div>
        </div>
        
        
        
        <script type="module" src="/src/js/ajoutDeSections/init.js"></script>
END;


        return $html;
    }

    private function tousLesQuestionnaires(){
        $routeur = $this->objets["routeur"];
        $questionnaires = $this->objets["questionnaires"];

        //on fabrique la liste des questionnaires
        $liste = "";
        foreach ($questionnaires as $q){
            $nom = $q->nom;
            $url = $routeur->pathFor("creationQuestionnaire", ["nom" => $nom, "id" => $q->idQuestionnaire]);
            $questio = <<<END
            <div class="col-md-12 pt-3">
                <a href="$url" class="sansDeco">
                    <div class="card">
                        <div class="card-body">
                            $nom <span class="oi oi-document float-right"></span></span>
                        </div>
                    </div>
                </a>
            </div>
END;
            $liste .= $questio;
        }

        //on fabrique le titre
        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p class="titre"> Voici tous vos questionnaires</p>
        </div>
        <div class="row">
            $liste
        </div>     
END;

        return $html;
    }

    public function render($param){
        switch ($param){
            case 1:
                $contenu = $this->nouveauQuestionnaireBase();
                break;
            case 2:
                $contenu = $this->nouveauQuestionnaire();
                break;
            case 3:
                $contenu = $this->tousLesQuestionnaires();
                break;

        }

        echo <<<END
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <title>Questionnaire</title>
                <link href="../../../bootstrap/css/bootstrap.css" rel="stylesheet">
                <link href="../../../open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
                <link href="../../../bootstrap/css/questionnaire.css" rel="stylesheet">
                <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
                <script src="../../../bootstrap/js/bootstrap.js"></script>
                <script src="../../../../node_modules/axios/dist/axios.js"></script>
            </head>
            
            <body>
                <div class="container col-md-8">
                    $contenu
                </div>
              
            </body>
        </html>
END;

    }
}