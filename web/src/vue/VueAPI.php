<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 23/04/2019
 * Time: 14:03
 */

namespace test\vue;


class VueAPI {
    private $tab;

    public function __construct($tableau){
        $this->tab = $tableau;
    }

    private function affichage(){
        //on met à jour la réponse
        $this->tab["reponse"] = $this->tab["reponse"]->withStatus(200)->withHeader("Content-Type", "application/json");

        //on retourne les données
        return json_encode($this->tab["donnees"]);
    }

    public function render($param){
        switch ($param){
            case 1:
                $content = $this->affichage();
                break;
        }

        echo $content;
        return $this->tab["reponse"];
    }
}