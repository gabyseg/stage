<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 09/05/2019
 * Time: 14:52
 */

namespace test\vue;


class ConnexionVue{
    private $objets;

    public function __construct($donnees){
        $this->objets = $donnees;
    }

    private function afficherPortailConnexionQuestionnaire(){
        //on récupère l'id
        $id = $this->objets["id"];

        if(isset($this->objets["nom"])){
            $phrase = "Connexion pour le formulaire : " . $this->objets["nom"];
        }else{
            $phrase = "Connexion pour le formulaire n° " . $id;
        }

        //on regarde si on s'est connecté avant et qu'on s'est trompé
        if(isset($this->objets["connexionOk"])){
            $m = $this->objets["message"];
            $message = "<p id=\"messageErreur\" style='display: inline-block'>$m<span class=\"oi oi-circle-x\"></span></p>";
        }else{
            $message = "";
        }

        //on fabrique le titre
        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p id ="titre" class="titre">$phrase</p>
        </div>
        <form method="post" action="" id="f1">
            <div class="form-group">
                <label for="motDePasse">Entrez ici le mot de passe :</label>
                <input id="motDePasse" class="form-control" type="password" required name="mdp"/>
            </div>
            <button type="submit" class="btn btn-danger" name="valider">Valider</button>
            $message
        </form>
END;

        return $html;
    }

    private function afficherPortailInscriptionRiver(){
        //on regarde si le pseudo a déjà été utilisé
        if($this->objets != null){
            //pour le message
            $m = $this->objets;
            $message = "<p id=\"messageErreur\" style='display: inline-block'>$m<span class=\"oi oi-circle-x\"></span></p>";
        }else{
            $message = "";
        }

        //on fabrique le titre
        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p id ="titre" class="titre"> Inscription RIVER</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <img id="imageRiver" src="../img/river.png" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="row pt-5" id="infoConnexion">
            <div class="col-md-12">
                <form class="col-md-12" method="post">
                    <div class="form-group">
                        <label for="pseudo">Pseudonyme</label>
                        <input class="form-control" type="text" placeholder="Nom d'utilisateur" name="pseudo" required>
                    </div>
                    <div class="form-group">
                        <label for="motDePasse1">Mot de passe</label>
                        <input type="password" class="form-control" id="motDePasse1" placeholder="Mot de passe" name="motDePasse" required>
                    </div>
                    <div class="form-group">
                        <label for="motDePasse2">Répétez</label>
                        <input type="password" class="form-control" id="motDePasse2" placeholder="Répétez le mot de passe" required>
                    </div>
                    <button type="submit" class="btn btn-danger" name="valider" value="valider">S'inscrire</button> $message
                    <p id="messageErreur" style='display: none'>test<span class="oi oi-circle-x"></span></p>
                </form>
            </div>
        </div>
        <script type="text/javascript" src="../src/js/verifierConnexion/verifierConnexion.js"></script>

END;

        return $html;
    }

    private function afficherPortailConnexionRiver(){
        //on récupère le routeur
        $routeur = $this->objets["routeur"];
        $url = $routeur->pathFor("inscriptionRiver");


        //on regarde si il y a déjà eu une tentative de connexion
        if(isset($this->objets["connexionOk"])){
            $m = $this->objets["message"];
            $message = "<p id=\"messageErreur\" style='display: inline-block'>$m<span class=\"oi oi-circle-x\"></span></p>";
        }else{
            $message = "";
        }


        //on fabrique le titre
        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p id ="titre" class="titre"> Connexion RIVER</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <img id="imageRiver" src="../img/river.png" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="row pt-5" id="infoConnexion">
            <div class="col-md-12">
                <form class="col-md-12" method="post">
                    <div class="form-group">
                        <label for="pseudo">Pseudonyme</label>
                        <input class="form-control" type="text" placeholder="Nom d'utilisateur" name="pseudo" required>
                    </div>
                    <div class="form-group">
                        <label for="motDePasse">Mot de passe</label>
                        <input type="password" class="form-control" id="motDePasse" placeholder="Mot de passe" name="mdp" required>
                    </div>
                    <button type="submit" class="btn btn-danger" name="valider" value="valider">Connexion</button>
                    <a href="$url" id="lienInscription">Inscription</a>
                    $message
                </form>
            </div>
        </div>
END;

        return $html;
    }

    private function afficherMenu(){
        //le pseudo
        $pseudo = $_SESSION["pseudo"];
        $routeur = $this->objets;
        $url = $routeur->pathFor("nouveauQuestionnaire");
        $url2 = $routeur->pathFor("afficherAnciensQuestionnaires");
        $urlDeconnexion = $routeur->pathFor("deconnexion");

        //on fabrique le titre
        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p id ="titre" class="titre"> Bienvenu $pseudo</p>
        </div>
        <div class="row">
            <h4>Que voulez vous faire ?</h4>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="$url" class="sansDeco">
                    <div class="card">
                        <div class="card-body">
                           Créer un nouveau questionnaire <span class="oi oi-align-left float-right"></span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-12 pt-3">
                <a href="$url2" class="sansDeco">
                    <div class="card">
                        <div class="card-body">
                           Voir mes questionnaires <span class="oi oi-spreadsheet float-right"></span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-12 pt-3">
                <a href="$urlDeconnexion" class="sansDeco">
                    <div class="card">
                        <div class="card-body">
                           Se déconnecter<span class="oi oi-account-logout float-right"></span></span>
                        </div>
                    </div>
                </a>
            </div>
            
            
        </div>
END;

        return $html;
    }

    public function render($param){
        switch ($param){
            case 1:
                $contenu = $this->afficherPortailConnexionQuestionnaire();
                break;
            case 2:
                $contenu = $this->afficherPortailInscriptionRiver();
                break;
            case 3:
                $contenu = $this->afficherPortailConnexionRiver();
                break;
            case 4;
                $contenu = $this->afficherMenu();
                break;
        }

        echo <<<END
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <title>Questionnaire</title>
                <link href="../../bootstrap/css/bootstrap.css" rel="stylesheet">
                <link href="../../open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
                <link href="../../bootstrap/css/questionnaire.css" rel="stylesheet">
                <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
            </head>
            
            <body>
                <div class="container col-md-8">
                    $contenu
                </div>
            </body>
        </html>
END;

    }
}