<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 28/05/2019
 * Time: 10:16
 */

namespace test\vue;


use test\modele\Variable;

class SectionVue{
    private $objets;

    public function __construct($donnees){
        $this->objets = $donnees;
    }

    public function afficherSection(){
        //on récupère la section
        $section = $this->objets["section"];
        $nom = $section->intitule;
        $route = $this->objets["routeArriere"];

        //on construit ici les questions déjà faites
        $questionsDejaFaites = "";
        foreach ($this->objets["questions"] as $q){
            //pour les réponses
            $listeRep = "";
            foreach ($q->rep as $r){
                $n = $r->intitule;
                $listeRep .= "<li>$n</li>";
            }

            //on construit l'entête de la page
            $intitule = $q->intitule;
            $id =  $q->idQuestion;
            $v = Variable::find($q->idVariable);
            $n = $v->nom;

            $res = <<<END
            <div>
                <div id="q$id" class="col-md">
                    <h5>$intitule : $n<span class="oi oi-delete float-right delete"></span></h5>
                </div>
                <div class="col-lg ml-5">
                    <ul class="listeDecoree">
                        $listeRep
                    </ul>
                </div>
            </div>
END;
            $questionsDejaFaites .= $res;
        }

        $html = <<<END
        <div class="row bordureTitre mt-2 mb-2">
            <p id ="titre" class="titre">$nom</p>
        </div>
        <div id="questions">
            $questionsDejaFaites
        </div>
        <div id="ajoutQuestions">
            <div id="boutonAjoutQuestion" data-toggle="modal" data-target="#ajoutQuestion">
                <p>Ajouter une question <span class="oi oi-plus"></span></p>
            </div>
        </div>
        
        <p>
	        <a href="$route" id="retourEnArriere"><span class="oi oi-action-undo"></span>  Retour en arrière</a>
        </p>

        <!-- Fenêtre modale -->
        <div class="modal fade" id="ajoutQuestion" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog-centered modal-dialog modal-lg" role="document">
                <div class="modal-content bordureRouge">
                
                    <div class="modal-header">
                        <div class="text-center col-md">
                            <h4 class="modal-title">Ajouter une nouvelle question</h4>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="question" id="nouvelleQuestion" value="nouvelleQuestion" checked>
                            <label class="form-check-label" for="nouvelleQuestion">
                                Nouvelle question
                            </label>
                        </div>
                        <div class="form-check pb-3">
                            <input class="form-check-input" type="radio" name="question" id="reutiliserQuestion" value="reutiliserQuestion">
                            <label class="form-check-label" for="reutiliserQuestion">
                               Réutiliser une question
                            </label>
                        </div>
                        
                        <div id="quest">
                            <div class="form-group">
                                <label for="nouvelElement">Nouvelle question</label>
                                <textarea class="form-control" id="nomNouvelleQuestion" rows="3" placeholder="Nouvelle question"></textarea>
                            </div>
                            
                            <hr>
                            
                            <div class="form-group">
                                <label for="variable">Variable :</label>
                                <input class="form-control" id="variable" type="text" placeholder="Nom de la variable" />
                            </div>
                            
                            <hr>
                            <div id="reponses">
                                <p>Réponses possibles</p>
                                <div id="listeReponses">
                                
                                </div>
                                <button class="btn btn-primary btn-sm" id="ajouterReponse">Ajouter une réponse</button>
                            </div>
                        </div>
                      
                        
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="annuler"> Annuler </button>
                        <button id="ajouterQuestion" type="button" class="btn btn-success" data-dismiss="modal">Ajouter</button>
                    </div>
                
                </div>
            </div>
        </div>

        <script type="module" src="/src/js/ajoutDeSections/initSection.js"></script>
END;

        return $html;
    }


    public function render($param){
        switch ($param){
            case 1:
                $content = $this->afficherSection();
                break;

        }

        echo <<<END
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <title>Questionnaire</title>
                <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
                <link href="../../../bootstrap/css/bootstrap.css" rel="stylesheet">
                <link href="../../../open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
                <link href="../../../bootstrap/css/questionnaire.css" rel="stylesheet">
                <script src="../../../bootstrap/js/bootstrap.js"></script>
                <script src="../../../../node_modules/axios/dist/axios.js"></script>
            </head>
            
            <body>
                <div class="container col-md-8">
                    $content
                </div>
              
            </body>
        </html>
END;

    }
}