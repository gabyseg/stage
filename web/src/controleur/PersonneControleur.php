<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 14/05/2019
 * Time: 10:01
 */

namespace test\controleur;


use test\modele\Personne;
use test\vue\VueAPI;

class PersonneControleur{
    public function getIdPersonne($container, $reponse){
        //on créé une nouvelle personne
        $personne = new Personne();
        $personne->save();

        //le tableau
        $tabDonnes = ["idPersonne" => $personne->idPersonne];
        $tab["reponse"] = $reponse;
        $tab["donnees"] = $tabDonnes;

        //la session
        $_SESSION["idPersonne"] = $personne->idPersonne;

        //on créé la vue de la réponse
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }
}