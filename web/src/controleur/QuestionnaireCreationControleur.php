<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 21/05/2019
 * Time: 15:00
 */

namespace test\controleur;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Container;
use Slim\Http\Response;
use test\modele\Questionnaire;
use test\vue\QuestionnaireVue;
use Illuminate\Database\Capsule\Manager as DB;

class QuestionnaireCreationControleur {
    public function nouveauQuestionnaire(Container $c, Response $r){

        //on construit la vue
        $vue = new QuestionnaireVue(null);
        $vue->render(1);
    }

    public function traiterNouveauQuestionnaire(Container $c, Response $r){
        //on vérifie qu'on vient juste de commencer un nouveau questionnaire
        if(isset($_POST["informationsDeBase"])){
            //on créé un nouveau questionnaire
            $questionnaire = new Questionnaire();
            $questionnaire->nom = $_POST["nom"];
            $questionnaire->mdp = password_hash($_POST["motDePasse"], PASSWORD_DEFAULT);
            $questionnaire->actif = false;
            $questionnaire->numCompte = $_SESSION["id"];

            $token = random_bytes(32);
            $token = bin2hex($token);
            $questionnaire->token = $token;

            //on sauvegarde
            $questionnaire->save();

            //on enregistre le numéro du questionnaire en cours dans la session
            $_SESSION["questionnaireEnCours"] = $questionnaire->idQuestionnaire;


            //on redirige
            $routeur = $c["router"];
            $url = $routeur->pathFor("creationQuestionnaire", ["nom" =>$_POST["nom"], "id" => $questionnaire->idQuestionnaire]);
            return $r->withRedirect($url);
        }
    }

    public function suppressionQuestionnaire(Container $c, Response $r, $args){
        if(isset($_SESSION["id"])){
            //on supprime le questionnaire
            $q = Questionnaire::find($args["id"]);
            $q->delete();

            $routeur = $c["router"];
            $url = $routeur->pathFor("afficherAnciensQuestionnaires");
            return $r->withRedirect($url);
        }
    }
    public function creationQuestionnaire(Container $c, Response $r, $args){
        //on va vérifier que la personne est bien connectée
        if(isset($_SESSION["id"])){
            try{
                //on va maintenant vérifier qu'il a accès à ce questionnaire
                $questionnaire = Questionnaire::findOrFail($args["id"]);
                if($questionnaire->numCompte == $_SESSION["id"]){
                    //on enregistre le numéro du questionnaire en cours dans la session
                    $_SESSION["questionnaireEnCours"] = $questionnaire->idQuestionnaire;
                    $tab["nom"] = $args["nom"];

                    //on y ajoute ses sections
                    $sections = $questionnaire->sections()->get();
                    $elem = $questionnaire->elementsCompares()->get();
                    $tab["sections"] = $sections;
                    $tab["elements"] = $elem;
                    $tab["questionnaire"] = $questionnaire;
                    $tab["routeur"] = $c["router"];

                    //ici on sélectionne tous les contacts
                    $contacts = DB::table("Contact")
                                    ->select("nom", "prenom", "mail")
                                    ->join("Personne", "Personne.idContact", "=", "Contact.idContact")
                                    ->join("Reponse", "Reponse.idPersonne", "=", "Personne.idPersonne")
                                    ->where("Reponse.idQuestionnaire", "=", $questionnaire->idQuestionnaire)
                                    ->distinct("nom", "prenom", "mail")
                                    ->get();

                    $tab["contacts"] = $contacts;

                    $vue = new QuestionnaireVue($tab);
                    $vue->render(2);
                }

            }catch (ModelNotFoundException $e){
                //si on ne le trouve pas on déclenche une erreur
                $reponse = $r->withStatus(404);
                $reponse = $r->withHeader("Content-Type", "application/json");
                echo json_encode(['error'=> 404, 'message'=>'not_found']);
                return $r;
            }
        }
    }

    public function anciensQuestionnaires(Container $container, $rep, $args){
        if(isset($_SESSION["id"])){
            //on récupère tous les anciens questionnaires
            $questionnaires = Questionnaire::where("numCompte", "=", $_SESSION["id"])->get();

            $routeur = $container["router"];
            $tab["routeur"] = $routeur;
            $tab["questionnaires"] = $questionnaires;

            //on appelle la vue
            $vue = new QuestionnaireVue($tab);
            $vue->render(3);
        }
    }

    public function traiterInfosContact(Container $container, $rep, $args){
        if(isset($_SESSION["id"])){
            if(isset($_SESSION["questionnaireEnCours"])){
                $questionnaire = Questionnaire::find($_SESSION["questionnaireEnCours"]);

                //on récupère les données
                $donnees = $_POST["donnees"];
                $donnees = json_decode($donnees);

                if($donnees->name == "resultats"){
                    if($donnees->valeur == "oui") $questionnaire->resultats = 1;
                    else $questionnaire->resultats = 0;
                }else{
                    if($donnees->valeur == "oui") $questionnaire->entretien = 1;
                    else $questionnaire->entretien = 0;
                }

                //on sauvegarde
                $questionnaire->save();
            }
        }
    }


}