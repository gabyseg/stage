<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 25/04/2019
 * Time: 09:54
 */

namespace test\controleur;


use Slim\Container;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use test\modele\Methode;
use test\modele\Profession;
use test\modele\Question;
use test\modele\Questionnaire;
use test\modele\Reponse;
use test\modele\ReponsePossible;
use test\modele\Section;
use test\modele\Specialite;
use test\modele\Variable;
use test\vue\VueAPI;

class QuestionControleur{
    public function questionDetaillee($args, Container $c, $reponse){
        try{
            //on récupère la question
            $question = Question::findOrFail($args["id"]);
            $tab["donnees"]["question"] = $question;

            //on va récupérer les réponses possibles
            $possibles = $question->reponsesPossibles()->get();

            //on enlève le pivot
            foreach ($possibles as $p){
                unset($p->pivot);
            }

            //si il y a des réponses possibles
            if(!$possibles->isEmpty()){
                //on cherche le type
                //$type = TypeDeReponse::findOrFail($possibles->first()["idType"]);

                //si on compare les méthodes
               // if(strpos($type->nom, "Comparaison") !== false){
                    //on récupère les méthodes
                    if(isset($_SESSION["questionnaireVoulu"])){
                        $id = $_SESSION["questionnaireVoulu"];
                        $questionnaire = Questionnaire::find($id);
                        $tab["donnees"]["methodes"] = $questionnaire->elementsCompares;

                    }else{
                        $methodes = Methode::all();
                        $tab["donnees"]["methodes"] = $methodes;
                    }

               // }

               /* //si on demande la profession
                if($type->nom == "Profession"){
                    $possibles = Profession::all();
                }

                //si on demande la Spécialité
                if($type->nom == "Spécialité"){
                    $possibles = Specialite::all();
                }*/

                //on met le type dans le tableau
               // $tab["donnees"]["type"] = $type;
            }

            //on passe les données
            $tab["donnees"]["reponsesPossibles"] = $possibles;

            //on passe la réponse
            $tab["reponse"] = $reponse;

        }catch (ModelNotFoundException $e){
            //si on ne le trouve pas on déclenche une erreur
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }

        //on instancie la vue
        $vue = new VueAPI($tab);
        $vue->render(1);
    }

    public function insererNouvelleQuestion(Container $c, $rep, $args){
        if(isset($_SESSION["id"]) && isset($_SESSION['questionnaireEnCours'])){
            //on récupère le questionnaire
            $questionnaire = Questionnaire::find($_SESSION["questionnaireEnCours"]);

            //l'id de la section
            $id = $_SESSION["idSectionEnCours"];
            $section = Section::find($id);

            //récupération des données
            $donnees = $_POST["donnees"];
            //on décode
            $donnees = json_decode($donnees);

            if($donnees->mode == "nouveau"){
                //on cherche si la variable existe ou non
                if(sizeof(Variable::where("idCompte", "=", $_SESSION["id"])->where("nom", "=", $donnees->variable)->get()) == 0){
                    $var = new Variable();
                    $var->idCompte = $_SESSION["id"];
                    $var->nom = $donnees->variable;
                    $var->save();
                }else{
                    $var = Variable::where("idCompte", "=", $_SESSION["id"])->where("nom", "=", $donnees->variable)->first();
                }

                //on enregistre la question
                $question = new Question();
                $question->intitule = $donnees->question;
                $question->idSection = $id;
                $question->idCompte = $_SESSION["id"];
                $question->idVariable = $var->id;
                $question->save();

                //lien avec possède
                $questionnaire->questions()->attach($question->idQuestion);
                $questionnaire->save();

                //on insère les réponses
                foreach ($donnees->reponses as $r){
                    $tempRep = new ReponsePossible();
                    $tempRep->intitule = $r;
                    $tempRep->idQuestion = $question->idQuestion;
                    $tempRep->save();
                }

                //on renvoie l'id de la question
                echo $question->idQuestion;
            }else{
                //on récupère le questionnaire
                $questionnaire = Questionnaire::find($_SESSION["questionnaireEnCours"]);
                $id = $donnees->id;
                $questionnaire->questions()->attach($id);


                $question = Question::find($id);
                $reponses = $question->reponsesPossibles()->get();

                $tab["reponses"] = $reponses;
                $tab["variable"] = $question->variable;

                echo json_encode($tab);
            }


        }
    }

    public function supprimerQuestion(Container $c, $rep, $args){
        if(isset($_SESSION["id"])){
            $idQ = $args["idQuestion"];
            $questio = Questionnaire::find($_SESSION["questionnaireEnCours"]);

            $questio->questions()->detach([$idQ]);
        }
    }

    public function getQuestions(Container $c, $rep, $args, $debut){
        if($debut == null){
            if(isset($_SESSION["id"])){
                $questions = Question::where("idCompte", "=", $_SESSION["id"])
                    ->where("idSection", "=" , $id = $_SESSION["idSectionEnCours"])
                    ->orderBy("intitule", "ASC")->get();

                //on passe la réponse
                $tab["reponse"] = $rep;
                $tab["donnees"] = $questions;

                //on instancie la vue
                $vue = new VueAPI($tab);
                return $vue->render(1);
            }
        }else{
            if(isset($_SESSION["id"])){
                $questions = Question::where("idCompte", "=", $_SESSION["id"])
                    ->where("idSection", "=" , $id = $_SESSION["idSectionEnCours"])
                    ->where("intitule", "like", "%$debut%")
                    ->orderBy("intitule", "ASC")->get();

                //on passe la réponse
                $tab["reponse"] = $rep;
                $tab["donnees"] = $questions;

                //on instancie la vue
                $vue = new VueAPI($tab);
                return $vue->render(1);
            }
        }

    }
}