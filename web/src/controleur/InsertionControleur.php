<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 09/05/2019
 * Time: 16:54
 */

namespace test\controleur;


use test\modele\Concerne;
use test\modele\Contact;
use test\modele\Methode;
use test\modele\Personne;
use test\modele\Profession;
use test\modele\Question;
use test\modele\Questionnaire;
use test\modele\Reponse;
use test\modele\ReponsePossible;
use test\modele\Section;
use test\modele\Specialite;

class InsertionControleur{
    public function insererComparaison($container, $reponse){
        try{
            //on regarde si on a bien les données
            if(isset($_POST["donnees"])){
                $donnees = $_POST["donnees"];

                $donnees = json_decode($donnees);

                $idQuestion = $donnees->idQuestion;
                $question = Question::find($idQuestion);
                $idQuestionnaire = $donnees->idQuestionnaire;
                $questionnaire = Questionnaire::find($idQuestionnaire);

                //pour chaque réponse
                foreach ($donnees->reponses as $rep){
                    //on récupère la méthode
                    $methode = $questionnaire->elementsCompares()->where("nomMethode", "=",$rep->nomMethode)->first();

                    //on récupère la réponse
                    //$repPos = ReponsePossible::where("intitule", "=", $rep->reponse)->first();
                    $repPos = $question->reponsesPossibles()->where("intitule", "=", $rep->reponse)->first();

                    //on vérifie qu'on a bien un résultat pour les deux
                    if(!empty($methode) && !empty($repPos)){
                        //on créé la nouvelle réponse
                        $reponse = new Reponse();
                        $reponse->idQuestionnaire = $idQuestionnaire;
                        $reponse->idQuestion = $idQuestion;
                        $reponse->idPersonne = $_SESSION["idPersonne"];
                        $reponse->save();

                        //création de l'objet concerne
                        $concerne = new Concerne();
                        $concerne->idReponse = $reponse->idReponse;
                        $concerne->idMethode = $methode->idMethode;
                        $concerne->idReponsePossible = $repPos->idReponse;
                        $concerne->save();
                    }else{
                        throw new \Exception();
                    }
                }
            }
        }catch (\Exception $e){
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }
    }

    public function insererProfession($container, $reponse){
        try{
            //on regarde si on a bien les données
            if(isset($_POST["donnees"])){
                $donnees = $_POST["donnees"];
                //on décode
                $donnees = json_decode($donnees);

                $pro = $donnees;

                if(isset($_SESSION["idPersonne"])){
                    $id = $_SESSION["idPersonne"];

                    $profession = Profession::where("intitule", "=" , $pro)->first();

                    if(!empty($profession)){
                        //on sélectionne la personne
                        $personne = Personne::findOrFail($id);
                        $personne->idProfession = $profession->idProfession;
                        $personne->save();
                    }
                }
            }
        }catch (\Exception $e){
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }
    }

    public function insererSpecialite($container, $reponse){
        try{
            //on regarde si on a bien les données
            if(isset($_POST["donnees"])){
                $donnees = $_POST["donnees"];
                //on décode
                $donnees = json_decode($donnees);

                $spe = $donnees;

                var_dump($donnees);

                if(isset($_SESSION["idPersonne"])){
                    $id = $_SESSION["idPersonne"];

                    $specialite = Specialite::where("intitule", "=" , $spe)->first();

                    if(!empty($specialite)){
                        //on sélectionne la personne
                        $personne = Personne::findOrFail($id);
                        $personne->idSpecialite = $specialite->idSpecialite;
                        $personne->save();
                    }
                }
            }
        }catch (\Exception $e){
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }
    }

    public function insererResultat($container, $reponse){
        try{
            //on regarde si on a bien les données
            if(isset($_POST["donnees"])){
                $donnees = $_POST["donnees"];
                //on décode
                $donnees = json_decode($donnees);

                $res = $donnees;

                if(isset($_SESSION["idPersonne"])){
                    $id = $_SESSION["idPersonne"];
                    //on sélectionne la personne
                    $personne = Personne::findOrFail($id);
                    if($res == "Oui"){
                        $personne->resultat = true;
                    }else{
                        $personne->resultat = false;
                    }
                    $personne->save();
                }
            }
        }catch (\Exception $e){
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }
    }

    public function insererEntretien($container, $reponse){
        try{
            //on regarde si on a bien les données
            if(isset($_POST["donnees"])){
                $donnees = $_POST["donnees"];
                //on décode
                $donnees = json_decode($donnees);

                $res = $donnees;

                if(isset($_SESSION["idPersonne"])){
                    $id = $_SESSION["idPersonne"];
                    //on sélectionne la personne
                    $personne = Personne::findOrFail($id);
                    if($res == "Oui"){
                        $personne->entretien = true;
                    }else{
                        $personne->entretien = false;
                    }
                    $personne->save();
                }
            }
        }catch (\Exception $e){
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }
    }

    public function insererContact($container, $reponse){
        try{
            //on regarde si on a bien les données
            if(isset($_POST["donnees"])){
                $donnees = $_POST["donnees"];
                //on décode
                $donnees = json_decode($donnees);

                $valeur = $donnees->valeur;
                $nom = $donnees->nom;

                if(isset($_SESSION["idPersonne"])){
                    $id = $_SESSION["idPersonne"];
                    //on sélectionne la personne
                    $personne = Personne::findOrFail($id);

                    //si elle est déjà associée à un contact
                    if($personne->idContact != null){
                        $contact = Contact::findOrFail($personne->idContact);
                        $contact[$nom] = $valeur;
                        $contact->save();
                    }else{
                        $contact = new Contact();
                        $contact[$nom] = $valeur;
                        $contact->save();
                        $personne->idContact = $contact->idContact;
                        $personne->save();
                    }

                }
            }
        }catch (\Exception $e){
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }
    }

    public function insererMail($container, $reponse){
        try{
            //on regarde si on a bien les données
            if(isset($_POST["donnees"])){
                $donnees = $_POST["donnees"];
                //on décode
                $donnees = json_decode($donnees);

                $mail = $donnees;

                if(isset($_SESSION["idPersonne"])){
                    $id = $_SESSION["idPersonne"];
                    //on sélectionne la personne
                    $personne = Personne::findOrFail($id);

                    //si elle est déjà associée à un contact
                    if($personne->idContact != null){
                        $contact = Contact::findOrFail($personne->idContact);
                        $contact->mail = $mail;
                        $contact->save();
                    }else{
                        $contact = new Contact();
                        $contact->mail = $mail;
                        $contact->save();
                        $personne->idContact = $contact->idContact;
                        $personne->save();
                    }

                }
            }
        }catch (\Exception $e){
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }
    }
}