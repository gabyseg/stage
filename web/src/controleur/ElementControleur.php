<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 27/05/2019
 * Time: 09:55
 */

namespace test\controleur;


use Slim\Container;
use test\modele\Methode;
use test\modele\Questionnaire;
use test\modele\Section;
use test\vue\VueAPI;

class ElementControleur{
    public function ajouterNouvelElement(){
        //on vérifie qu'il y a bien un questionnaire en cours
        if(isset($_SESSION["questionnaireEnCours"])){
            $numQ = $_SESSION["questionnaireEnCours"];
            $ques = Questionnaire::findOrFail($numQ);

            $donnees = $_POST["donnees"];

            $donnees = json_decode($donnees);

            if($donnees->mode == "nouveau"){
                //on créé la nouvelle section
                $element = new Methode();
                $element->nomMethode = $donnees->intituleElement;
                $element->idCompte = $_SESSION["id"];
                $element->save();

                //on l'ajoute dans la table "a pour section"
                $ques->elementsCompares()->attach([$element->idMethode]);
                $ques->save();

                echo $element->idMethode;
            }else{
                $ques->elementsCompares()->attach([$donnees->id]);
            }

        }
    }

    public function supprimerElement($args){
        //on vérifie qu'il y a bien un questionnaire en cours
        if(isset($_SESSION["questionnaireEnCours"])){
            $numQ = $_SESSION["questionnaireEnCours"];
            $numE = $args["numElement"];

            //on détache de l'association
            $questionnaire = Questionnaire::findOrFail($numQ);
            $questionnaire->elementsCompares()->detach([$numE]);

            //on supprimer la section
           // $section = Methode::findOrFail($numE);
           // $section->delete();
        }
    }

    public function getElements(Container $c, $rep, $args, $debut){
        if($debut == null){
            if(isset($_SESSION["id"])){
                $sections = Methode::where("idCompte", "=", $_SESSION["id"])->orderBy("nomMethode", "ASC")->get();

                //on passe la réponse
                $tab["reponse"] = $rep;
                $tab["donnees"] = $sections;

                //on instancie la vue
                $vue = new VueAPI($tab);
                return $vue->render(1);
            }
        }else{
            if(isset($_SESSION["id"])){
                $sections = Methode::where("idCompte", "=", $_SESSION["id"])
                    ->where("nomMethode", "like", "%$debut%")
                    ->orderBy("nomMethode", "ASC")->get();

                //on passe la réponse
                $tab["reponse"] = $rep;
                $tab["donnees"] = $sections;

                //on instancie la vue
                $vue = new VueAPI($tab);
                return $vue->render(1);
            }
        }

    }


}