<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 23/04/2019
 * Time: 15:55
 */

namespace test\controleur;


use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Container;
use test\modele\Profession;
use test\modele\Question;
use test\modele\Questionnaire;
use test\modele\Section;
use test\modele\Specialite;
use test\vue\SectionVue;
use test\vue\VueAPI;
use Illuminate\Database\Capsule\Manager as DB;

class SectionControleur{
    public function sectionEtQuestions($args, Container $c, $reponse){
        //on récupère le questionnaire
        try{
            //on récupère le routeur
            $routeur = $c["router"];

            //pour vérfifier que la section existe
            $section = Section::findOrFail($args["idSection"]);

            //on récupère les questions de la section
            $questions = DB::table("Question")
                ->join("Possede", "Question.idQuestion", "=", "Possede.idQuestion")
                ->where("Question.idSection", "=", $args["idSection"])
                ->where("Possede.idQuestionnaire", "=", $args["idQuestionnaire"])
                ->get();

            //on parcourt les questions
            foreach ($questions as $q){
                //on ajoute le lien vers la question détaillée
                $lien = $routeur->pathFor("questionDetaille", ["id" => $q->idQuestion]);
                $q->links = ["details" => $lien];
                $q->links = ["details" => $lien];
            }

            //on passe les données
            $tab["donnees"]["section"] = $section;
            $tab["donnees"]["questions"] = $questions;

            //on va chercher le lien d'avant et d'après
            $avant = "";
            $apres = "";

            //on récupère toutes les sections concernant le questionanire
            $sections = DB::table("Section")
                ->join("aPourSection", "aPourSection.idSection", "=", "Section.idSection")
                ->where("idQuestionnaire", "=", $args["idQuestionnaire"])
                ->get();

            //on les parcourt
            foreach($sections as $key => $value){
                if($value->idSection == $args["idSection"]){
                    //quand on a la bonne section
                    //si il y en a un après
                    if(sizeof($sections) >= $key+2){
                        $apres = $routeur->pathFor("sectionDetaillee",array(
                            "idQuestionnaire" => $args["idQuestionnaire"],
                            "idSection" => $sections[$key+1]->idSection
                        ));
                    }
                    //si c'est le premier
                    if($key != 0){
                        //on met également celui d'avant
                        $avant = $routeur->pathFor("sectionDetaillee",array(
                            "idQuestionnaire" => $args["idQuestionnaire"],
                            "idSection" => $sections[$key-1]->idSection
                        ));
                    }
                }
            }



            //on créé le tableau de liens
            $links = array(
                "prev" => $avant,
                "next" => $apres
            );

            //on regarde si c'est la dernière
            if($links["next"] == ""){
                $links["next"] = "/api/sectionProfession";
            }

            $tab["donnees"]["links"] = $links;


            //on passe la réponse
            $tab["reponse"] = $reponse;

        }catch (ModelNotFoundException $e){
            //si on ne le trouve pas on déclenche une erreur
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'not_found']);
            return $reponse;
        }

        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }

    public function professions( Container $c, $reponse){
        //on récupère le routeur
        $routeur = $c["router"];

        //on récupère toutes les professions
        $tab["donnees"]["reponsesPossibles"] = Profession::all();
        $tab["donnees"]["question"] = ["idQuestion"=>"1","intitule" => "Quelle est votre profession?","idSection" => "",];
        $tab["reponse"] = $reponse;


        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }

    public function specialites(Container $c, $reponse){
        //on récupère le routeur
        $routeur = $c["router"];

        //on récupère toutes les professions
        $tab["donnees"]["reponsesPossibles"] = Specialite::all();
        $tab["donnees"]["question"] = ["idQuestion"=>"2","intitule" => "Dans quelle spécialité médicale (du corps humain) travaillez-vous?","idSection" => "",];
        $tab["reponse"] = $reponse;


        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }

    public function fabriquerSectionProfession($container, $rep){
        //on sélectionne le questionnaire
        $tab["donnees"] = [
            "section" => ["idSection" => "", "intitule" => "Profession"],
            "questions" => [
                [   "idQuestion"=>"1",
                    "intitule" => "Quelle est votre profession?",
                    "idSection" => "",
                    "idQuestionnaire" => "",
                    "links" => ["details" => "/api/professions"]
                ],
                [   "idQuestion"=>"2",
                    "intitule" => "Dans quelle spécialité médicale (du corps humain) travaillez-vous?",
                    "idSection" => "",
                    "idQuestionnaire" => "",
                    "links" => ["details" => "/api/specialites"]
                ]

            ],
            "links" => [
                "prev" => "",
                "next" => "",
            ]
        ];

        $tab["reponse"] = $rep;

        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }

    public function enregistrerNouvelleSection(Container $c, $response, $args){
        //on vérifie qu'il y a bien un questionnaire en cours
        if(isset($_SESSION["questionnaireEnCours"])){
            $numQ = $_SESSION["questionnaireEnCours"];
            $ques = Questionnaire::findOrFail($numQ);

            $donnees = $_POST["donnees"];

            $donnees = json_decode($donnees);

            if($donnees->mode == "nouveau"){
                //on créé la nouvelle section
                $section = new Section();
                $section->intitule = $donnees->intituleSection;
                $section->idCompte = $_SESSION["id"];
                $section->save();

                //on l'ajoute dans la table "a pour section"
                $ques = Questionnaire::findOrFail($numQ);
                $ques->sections()->attach([$section->idSection]);
                $ques->save();

                echo $section->idSection;
            }else{
                $ques->sections()->attach([$donnees->id]);
            }


        }
    }

    public function supprimerSection($args){
        //on vérifie qu'il y a bien un questionnaire en cours
        if(isset($_SESSION["questionnaireEnCours"])){
            $numQ = $_SESSION["questionnaireEnCours"];
            $numS = $args["numSection"];

            //on détache de l'association
            $questionnaire = Questionnaire::findOrFail($numQ);
            $questionnaire->sections()->detach([$numS]);

            //pour les questions
            //on sélectionne toutes les questions de la section voulue
            $questions = DB::table("Question")
                ->join("Possede", "Question.idQuestion", "=", "Possede.idQuestion")
                ->where("Question.idSection", "=", $numS)
                ->where("Possede.idQuestionnaire", "=", $numQ)
                ->get();

            $tab = [];
            foreach ($questions as $q){
                $tab[] = $q->idQuestion;
            }

            //on détache
            $questionnaire->questions()->detach($tab);


            //on supprimer la section
            $section = Section::findOrFail($numS);
           // $section->delete();
        }
    }

    public function modificationSection(Container $c, $rep, $args){
        if(isset($_SESSION["questionnaireEnCours"])){
            //on récupère le questionnaire
            $idQ = $_SESSION["questionnaireEnCours"];
            $q = Questionnaire::find($idQ);

            //on récupère la section et les questions
            //on récupère les questions de la section
            $questions = DB::table("Question")
                ->join("Possede", "Question.idQuestion", "=", "Possede.idQuestion")
                ->where("Question.idSection", "=", $args["idSection"])
                ->where("Possede.idQuestionnaire", "=", $idQ)
                ->get();

            //on va leur rajouter leurs réponses
            foreach ($questions as $qu){
                $questionDeBase = Question::find($qu->idQuestion);
                $reponses = $questionDeBase->reponsesPossibles()->get();
                $qu->rep = $reponses;
            }

            $tab["questions"] = $questions;
            $tab["section"] = Section::find($args["idSection"]);
            $routeur = $c["router"];
            $routeArriere = $routeur->pathFor("creationQuestionnaire", ["nom" => $q->nom, "id" =>$q->idQuestionnaire]);
            $tab["routeArriere"] = $routeArriere;

            //on ajoute en session la section en cours de modification
            $_SESSION["idSectionEnCours"] = $args["idSection"];

            //on passe le tout à la vue
            $vue = new SectionVue($tab);
            $vue->render(1);
        }
    }

    public function getSections(Container $c, $rep, $args, $deb){
        if($deb == null){
            if(isset($_SESSION["id"])){
                $sections = Section::where("idCompte", "=", $_SESSION["id"])->orderBy("intitule", "ASC")->get();

                //on passe la réponse
                $tab["reponse"] = $rep;
                $tab["donnees"] = $sections;

                //on instancie la vue
                $vue = new VueAPI($tab);
                return $vue->render(1);
            }
        }else{
            $sections = Section::where("idCompte", "=", $_SESSION["id"])
                ->where("intitule", "like", "%" . $deb . "%")
                ->orderBy("intitule", "ASC")->get();

            //on passe la réponse
            $tab["reponse"] = $rep;
            $tab["donnees"] = $sections;

            //on instancie la vue
            $vue = new VueAPI($tab);
            return $vue->render(1);
        }

    }
}