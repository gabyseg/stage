<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 23/04/2019
 * Time: 11:30
 */

namespace test\controleur;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Capsule\Manager as DB;
use Slim\App;
use Slim\Container;
use test\modele\Concerne;
use test\modele\Question;
use test\modele\Questionnaire;
use test\modele\Reponse;
use test\modele\ReponsePossible;
use test\modele\Variable;
use test\vue\ConnexionVue;
use test\vue\VueAPI;

class QuestionnaireControleur{

    public function questionnaireEtSections($id, Container $container, $reponse){
        //on récupère le routeur
        $routeur = $container->get("router");

        try{
            //on récupère le questionnaire correspondant
            $questionnaire = Questionnaire::findOrFail($id);
            $tab["donnees"]["questionnaire"] = $questionnaire;

            //on récupère les sections
            $sections = $questionnaire->sections()->get();
            //on supprime le "pivot"
            foreach ($sections as $s){
                unset($s->pivot);
                //on ajoute le lien vers ses détails
                $lien = $routeur->pathFor("sectionDetaillee", ["idQuestionnaire"=> $questionnaire->idQuestionnaire,
                    "idSection" => $s->idSection
                ]);
                $s->links = ["details" => $lien];
            }

            //on ajoute la section de la profession
            $url = $routeur->pathFor("sectionProfession");
            $sections->push([
                "idSection" => "",
                "intitule" => "Profession",
                "links" => [
                    "details" => $url,
                ]
            ]);


            $tab["donnees"]["sections"] = $sections;

            //pour la visualisation
            $url = $routeur->pathFor("projet", ["id"=>$questionnaire->idQuestionnaire, "token"=>$questionnaire->token]);
            $tab["donnees"]["representation"] = $url;

            //on passe la réponse
            $tab["reponse"] = $reponse;

        }catch (ModelNotFoundException $e){
            //si on ne le trouve pas on déclenche une erreur
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'questionnaire_not_found']);
            return $reponse;
        }


        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }

    public function questionnaireActif(Container $container, $reponse){
        //on récupère le routeur
        $routeur = $container->get("router");

        try{
            //on récupère le questionnaire correspondant
            $questionnaire = Questionnaire::where("actif", "=", "1")->first();

            //on récupère sa route
            $route = $routeur->pathFor("afficherQuestionnaire", ["id" => $questionnaire->idQuestionnaire]);
            $tab["donnees"]["chemin"] = $route;

            //on passe la réponse
            $tab["reponse"] = $reponse;

            return $reponse->withRedirect($route);

        }catch (ModelNotFoundException $e){
            //si on ne le trouve pas on déclenche une erreur
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            echo json_encode(['error'=> 404, 'message'=>'questionnaire_not_found']);
            return $reponse;
        }


        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }

    public function afficherConnexionQuestionnaire(Container $container, $reponse, $idQuestionnaire){
        $tab = [];
        $tab["id"] = $idQuestionnaire;
        try{
            $tab["nom"] = Questionnaire::findOrFail($idQuestionnaire)->nom;
        }catch (ModelNotFoundException $e){

        }

        //on instancie la vue
        $vue = new ConnexionVue($tab);
        $vue->render(1);
    }

    public function traiterConnexion(Container $container, $reponse, $args, $vue){
        //on commence par récupérer le questionnaire
        try{
            $id = $args["id"];
            $q = Questionnaire::findOrFail($id);

            //on vérifie le mot de passe
            if(isset($_POST["mdp"]) && password_verify($_POST["mdp"], $q->mdp)){
                //on enregistre dans la session
                $_SESSION["questionnaireVoulu"] = $id;

                try{
                    $tab["nom"] = Questionnaire::findOrFail($id)->nom;
                }catch (ModelNotFoundException $e){

                }

                //on redirige vers le questionnaire
                return $vue->render($reponse, 'src/html/section.php');


            }else{

                //si le mot de passe n'est pas le bon
                $tab = [];
                $tab["id"] = $id;
                $tab["connexionOk"] = false;
                $tab["message"] = "Mot de passe incorrect";

                try{
                    $tab["nom"] = Questionnaire::findOrFail($id)->nom;
                }catch (ModelNotFoundException $e){

                }

                //on instancie la vue
                $vue = new ConnexionVue($tab);
                $vue->render(1);
            }
        }catch (ModelNotFoundException $e){
            //dans le cas où le questionnaire n'existe pas
            $tab = [];
            $tab["id"] = $id;
            $tab["connexionOk"] = false;
            $tab["message"] = "Questionnaire inexistant";

            //on instancie la vue
            $vue = new ConnexionVue($tab);
            $vue->render(1);
        }
    }

    public function questionnaireVoulu(Container $container, $reponse){
        //on récupère le routeur
        $routeur = $container->get("router");
        if(isset($_SESSION["questionnaireVoulu"])){
            $val = $_SESSION["questionnaireVoulu"];
            //on récupère sa route
            $route = $routeur->pathFor("questionnaire", ["id" => $val]);

            $reponse = $reponse->withStatus(200);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            return json_encode($route);
        }else{
            $reponse = $reponse->withStatus(404);
            $reponse = $reponse->withHeader("Content-Type", "application/json");
            return json_encode(['error'=>404, 'message' => 'Pas de questionnaire voulu']);
        }
    }

    public function getRepresentation($c, $rep, $args){
        //on récupère le questionnaire et ses sections
        $questionnaire = Questionnaire::find($args["id"]);

        //on récupère les sections
        $sections = $questionnaire->sections;

        foreach ($sections as $s){
            //echo $s->idSection;
            //on récupère les questions de la section
            $questions = DB::table("Question")
                ->join("Possede", "Question.idQuestion", "=", "Possede.idQuestion")
                ->where("Question.idSection", "=", $s->idSection)
                ->where("Possede.idQuestionnaire", "=", $args["id"])
                ->get();

            //pour chaque question
            foreach ($questions as $q){
                //on sélectionne sa variable
                $variable = Variable::find($q->idVariable);
                $q->variable = $variable->nom;

                //on va aller chercher ses réponses possibles
                $repPossibles = ReponsePossible::where("idQuestion", "=", $q->idQuestion)->get()->toArray();
                //pour les scores
                $tabRep = [];
                $i = 0;

                //pour nsp et na
                $idNSP = -1;
                $idNa = -1;

                foreach ($repPossibles as $r){
                    if($r["intitule"] != "NSP" && $r["intitule"] != "NA"){
                        $tabRep[$r["idReponse"]] = $i;
                        $i++;
                    }else if($r["intitule"] == "NSP"){
                        $idNSP = $r["idReponse"];
                    }else if($r["intitule"] == "NA"){
                        $idNa = $r["idReponse"];
                    }
                }

                //pour les elements de comparaison
                $tabScore = [];
                foreach ($questionnaire->elementsCompares as $elem){
                    $tabScore[$elem->idMethode] = 0;
                }

                //maintenant il faut parcourir les réponses
                $reponses = Reponse::where("idQuestion", "=", $q->idQuestion)
                    ->join("Concerne", "Reponse.idReponse", "=", "Concerne.idReponse")
                    ->get();


                //pour les nombres de nsp et na
                $nbNSP = 0;
                $nbNA = 0;

                foreach ($reponses as $r){
                    if($r->idReponsePossible == $idNa){
                        $nbNA++;
                    }else if( $r->idReponsePossible == $idNSP){
                        $nbNSP++;
                    }else{
                        //on récupère le score
                        $score = $tabRep[$r->idReponsePossible];
                        //on ajoute dans le tableau
                        $tabScore[$r->idMethode] += $score;
                    }
                }

                //on modifie et on ne prend que la moyenne des scores
                $nbPersonnes = Reponse::where("idQuestion", "=", $q->idQuestion)
                    ->get();
                $nbPersonnes = sizeof($nbPersonnes)/2;
                foreach ($tabScore as $index => $value){
                    $tabScore[$index] = round($tabScore[$index]/$nbPersonnes, 2);
                }

                $scoresFinaux = [];
                //pour re avoir les noms
                foreach ($questionnaire->elementsCompares as $elem){
                    $scoresFinaux[$elem->nomMethode] = $tabScore[$elem->idMethode];
                }

                $q->scores = $scoresFinaux;
                $q->nbPersonnes = $nbPersonnes;
                $q->nsp = $nbNSP;
                $q->na = $nbNA;
                $q->noteMax = sizeof($tabRep)-1;
            }

            $s->questions = $questions;
        }


        $tab["donnees"]["sections"] = $sections;
        $tab["donnees"]["elements"] = $questionnaire->elementsCompares;
        $tab["reponse"] = $rep;

        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }


    public function getGraphiques($c, $rep, $args){
        //on récupère le questionnaire et ses sections
        $questionnaire = Questionnaire::find($_SESSION["afficherReprésentation"]);

        //on récupère les sections
        $sections = $questionnaire->sections;

        $listeQuestions = [];

        foreach ($sections as $s){
            //echo $s->idSection;
            //on récupère les questions de la section
            $questions = DB::table("Question")
                ->join("Possede", "Question.idQuestion", "=", "Possede.idQuestion")
                ->where("Question.idSection", "=", $s->idSection)
                ->where("Possede.idQuestionnaire", "=", $_SESSION["afficherReprésentation"])
                ->get();

            //pour chaque question
            foreach ($questions as $q){
                //on sélectionne sa variable
                $variable = Variable::find($q->idVariable);
                $q->variable = $variable->nom;

                //on va aller chercher ses réponses possibles
                $repPossibles = ReponsePossible::where("idQuestion", "=", $q->idQuestion)->get()->toArray();
                //pour les scores
                $tabRep = [];
                $i = 0;

                //pour nsp et na
                $idNSP = -1;
                $idNa = -1;
                $tabNSP = [];
                $tabNA = [];

                foreach ($repPossibles as $r){
                    if($r["intitule"] != "NSP" && $r["intitule"] != "NA"){
                        $tabRep[$r["idReponse"]] = $i;
                        $i++;
                    }else if($r["intitule"] == "NSP"){
                        $idNSP = $r["idReponse"];
                    }else if($r["intitule"] == "NA"){
                        $idNa = $r["idReponse"];
                    }
                }

                //pour les elements de comparaison
                $tabScore = [];
                foreach ($questionnaire->elementsCompares as $elem){
                    $tabScore[$elem->idMethode] = 0;
                    $tabNSP[$elem->idMethode] = 0;
                    $tabNA[$elem->idMethode] = 0;
                }

                //maintenant il faut parcourir les réponses
                $reponses = Reponse::where("idQuestion", "=", $q->idQuestion)
                    ->join("Concerne", "Reponse.idReponse", "=", "Concerne.idReponse")
                    ->get();


                //pour les nombres de nsp et na
                $nbNSP = 0;
                $nbNA = 0;

                foreach ($reponses as $r){
                    if($r->idReponsePossible == $idNa){
                        $tabNA[$r->idMethode]++;
                        $nbNA++;
                    }else if( $r->idReponsePossible == $idNSP){
                        $tabNSP[$r->idMethode]++;
                        $nbNSP++;
                    }else{
                        //on récupère le score
                        $score = $tabRep[$r->idReponsePossible];
                        //on ajoute dans le tableau
                        $tabScore[$r->idMethode] += $score;
                    }
                }

                //on modifie et on ne prend que la moyenne des scores
                $nbPersonnes = Reponse::where("idQuestion", "=", $q->idQuestion)
                    ->get();
                $nbPersonnes = sizeof($nbPersonnes)/2;
                foreach ($tabScore as $index => $value){
                    $tabScore[$index] = round($tabScore[$index]/($nbPersonnes-$tabNSP[$index]-$tabNA[$index]), 2);
                }

                $scoresFinaux = [];
                $tabNSPFinal = [];
                $tabNAFinal = [];
                //pour re avoir les noms
                foreach ($questionnaire->elementsCompares as $elem){
                    $scoresFinaux[$elem->nomMethode] = $tabScore[$elem->idMethode];
                    $tabNSPFinal[$elem->nomMethode] = $tabNSP[$elem->idMethode];
                    $tabNAFinal[$elem->nomMethode] = $tabNA[$elem->idMethode];
                }

                $q->scores = $scoresFinaux;

                //on contruit l'élément ici
                $element = [];
                $element["labels"] = [$q->intitule];
                $element["datasets"] = [];
                $element["tableauNSP"] = $tabNSPFinal;
                $element["tableauNA"] = $tabNAFinal;

                //on remplir les datasets
                foreach ($q->scores as $cle => $val){
                    $temp = [];
                    $temp["label"] = $cle;
                    $temp["data"] = [$val];

                    //on rajoute le score à l'élément
                    $element["datasets"][] = $temp;
                }

                $noteMax = sizeof($tabRep)-1;

                //on ajoute la question dans la liste
                $tabTemp["elements"] = $element;
                $tabTemp["noteMax"] = $noteMax;

                $listeQuestions[] = $tabTemp;
            }
        }


        $tab["donnees"] = $listeQuestions;
        $tab["reponse"] = $rep;

        //on instancie la vue
        $vue = new VueAPI($tab);
        return $vue->render(1);
    }

    public function getCSV($args){
        $questionnaire = Questionnaire::find($args["num"]);

        //on construit d'abord la légende
        {
            //pour le tableau de la fin
            $tabFinal = [array("Etape", "Question", "variable")];

            //on y ajoute des éléments
            foreach ($questionnaire->elementsCompares as $e){
                $tabFinal[0][] = $e->nomMethode;
            }

            array_push($tabFinal[0], "1ere valeure", "2eme valeur", "Serpent", "Grenouille 1", "Grenouille 2");
        }

        //les infos qui vont nous aider
        {
            //on récupère le questionnaire et ses sections
            $questionnaire = Questionnaire::find($args["num"]);

            //on récupère les sections
            $sections = $questionnaire->sections;

            $listeQuestions = [];

            foreach ($sections as $s){
                //echo $s->idSection;
                //on récupère les questions de la section
                $questions = DB::table("Question")
                    ->join("Possede", "Question.idQuestion", "=", "Possede.idQuestion")
                    ->where("Question.idSection", "=", $s->idSection)
                    ->where("Possede.idQuestionnaire", "=", $args["num"])
                    ->get();

                //pour chaque question
                foreach ($questions as $q){
                    //on sélectionne sa variable
                    $variable = Variable::find($q->idVariable);
                    $q->variable = $variable->nom;

                    //on va aller chercher ses réponses possibles
                    $repPossibles = ReponsePossible::where("idQuestion", "=", $q->idQuestion)->get()->toArray();
                    //pour les scores
                    $tabRep = [];
                    $i = 0;

                    //pour nsp et na
                    $idNSP = -1;
                    $idNa = -1;

                    foreach ($repPossibles as $r){
                        if($r["intitule"] != "NSP" && $r["intitule"] != "NA"){
                            $tabRep[$r["idReponse"]] = $i;
                            $i++;
                        }else if($r["intitule"] == "NSP"){
                            $idNSP = $r["idReponse"];
                        }else if($r["intitule"] == "NA"){
                            $idNa = $r["idReponse"];
                        }
                    }

                    //pour les elements de comparaison
                    $tabScore = [];
                    foreach ($questionnaire->elementsCompares as $elem){
                        $tabScore[$elem->idMethode] = 0;
                    }

                    //maintenant il faut parcourir les réponses
                    $reponses = Reponse::where("idQuestion", "=", $q->idQuestion)
                        ->join("Concerne", "Reponse.idReponse", "=", "Concerne.idReponse")
                        ->get();


                    //pour les nombres de nsp et na
                    $nbNSP = 0;
                    $nbNA = 0;

                    foreach ($reponses as $r){
                        if($r->idReponsePossible == $idNa){
                            $nbNA++;
                        }else if( $r->idReponsePossible == $idNSP){
                            $nbNSP++;
                        }else{
                            //on récupère le score
                            $score = $tabRep[$r->idReponsePossible];
                            //on ajoute dans le tableau
                            $tabScore[$r->idMethode] += $score;
                        }
                    }

                    //on modifie et on ne prend que la moyenne des scores
                    $nbPersonnes = Reponse::where("idQuestion", "=", $q->idQuestion)
                        ->get();
                    $nbPersonnes = sizeof($nbPersonnes)/2;
                    foreach ($tabScore as $index => $value){
                        if($nbPersonnes != 0){
                            $tabScore[$index] = round($tabScore[$index]/$nbPersonnes, 2);
                        }
                    }

                    $scoresFinaux = [];
                    //pour re avoir les noms
                    foreach ($questionnaire->elementsCompares as $elem){
                        $scoresFinaux[$elem->nomMethode] = $tabScore[$elem->idMethode];
                    }

                    $noteMax = sizeof($tabRep)-1;

                    $q->scores = $scoresFinaux;
                    $q->section = $s->intitule;
                    $q->noteMax = $noteMax;
                    $listeQuestions[] = $q;
                }
            }
        }

        //ici on formate nos données
        {
            //pour chaque question
            foreach ($listeQuestions as $q){
                $temp = [];

                //pour les accents
                $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                    'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                    'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                    'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                    'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );


                array_push($temp, strtr( $q->section, $unwanted_array ), strtr($q->intitule, $unwanted_array ), strtr($q->variable, $unwanted_array ));

                //pour les scores
                $maxNom = "";
                $maxScore = -1000;

                //on recherche le 1er
                foreach ($q->scores as $key => $score){
                    if($score > $maxScore) {
                        $maxNom = $key;
                        $maxScore = $score;
                    }

                    $temp[] = $score;
                }

                $deuxiemeNom = "";
                $deuxiemeScore = -1000;
                //on recherche le deuxième
                foreach ($q->scores as $key => $score){
                    if($score > $deuxiemeScore && $key!=$maxNom) {
                        $deuxiemeNom = $key;
                        $deuxiemeScore = $score;
                    }
                }

                //on ajoute les deux meilleurs scores
                array_push($temp, $maxScore, $deuxiemeScore);


                //pour les serpents etc
                if($maxScore < $q->noteMax/2){
                    array_push($temp, "Vrai", "/", "/");
                }else{
                    $temp[] = "/";
                    if($maxScore > $deuxiemeScore*1.2){
                        //une grenouille
                        array_push($temp, $maxNom, "/");
                    }else{
                        //deux
                        array_push($temp, $maxNom, $deuxiemeNom);
                    }
                }

                $tabFinal[] = $temp;
            }
        }


       $this->array_to_csv_download($tabFinal, // this array is going to be the second row
            "numbers.csv"
       );

    }

    function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        foreach ($array as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }
}