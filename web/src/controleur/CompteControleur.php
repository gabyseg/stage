<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 20/05/2019
 * Time: 15:36
 */

namespace test\controleur;


use Slim\Container;
use Slim\Http\Response;
use test\modele\Compte;
use test\vue\ConnexionVue;

class CompteControleur{
    public function afficherInscription(Container $c, Response $r){
        //on créé la vue
        $vue = new ConnexionVue(null);
        $vue->render(2);
    }

    public function traiterInscription(Container $c, Response $r){
        //on récupère le routeur
        $router = $c["router"];

        //si il est connecté
        if(isset($_SESSION["id"])){
            $url = $router->pathFor("menuRiver");
            return $r->withRedirect($url);
        }else{
            //on vérifie qu'on provient bien du bon questionnaire
            if(isset($_POST["valider"])){
                //on regarde si le nom n'est pas déjà utilisé
                $compte = Compte::where("pseudo", "=", $_POST["pseudo"])->get();
                if(sizeof($compte) == 0){
                    //on créé un nouvel utilisateur
                    $nouvUtil = new Compte();
                    $nouvUtil->pseudo = $_POST["pseudo"];
                    $nouvUtil->mdp = password_hash($_POST["motDePasse"], PASSWORD_DEFAULT);
                    $nouvUtil->save();

                    $url = $router->pathFor("connexionRiver");

                    return $r->withRedirect($url);
                }else{
                    //si le pseudo est déjà pris on l'en informe
                    //on créé la vue
                    $vue = new ConnexionVue("Le pseudo est déjà utilisé");
                    $vue->render(2);
                }
            }else{
                return $r;
            }
        }

    }

    public function afficherConnexion(Container $c, Response $r){
        //on récupère le routeur
        $routeur = $c->get("router");
        $tab["routeur"] = $routeur;

        if(isset($_SESSION["id"])){
            $url = $routeur->pathFor("menuRiver");
            return $r->withRedirect($url);
        }else{
            //on créé la vue
            $vue = new ConnexionVue($tab);
            $vue->render(3);
        }
    }

    public function traiterConnexion(Container $c, Response $r){
        //on récupère le routeur
        $routeur = $c->get("router");

        //on récupère les informations
        $pseudo = $_POST["pseudo"];
        $mdp = $_POST["mdp"];

        //on vérifie les informations
        try{
            //on récupère le compte
            $compte = Compte::where("pseudo", "=", $pseudo)->first();
            if(empty($compte)){
                $tab["routeur"] = $routeur;
                $tab["connexionOk"] = false;
                $tab["message"] = "Le compte n'existe pas";

                //on créé la vue
                $vue = new ConnexionVue($tab);
                $vue->render(3);
            }else{
                if(password_verify($mdp, $compte->mdp)){
                    $_SESSION["id"] = $compte->idCompte;
                    $_SESSION["pseudo"] = $compte->pseudo;

                    $url = $routeur->pathFor("menuRiver");
                    return $r->withRedirect($url);
                }else{
                    $tab["routeur"] = $routeur;
                    $tab["connexionOk"] = false;
                    $tab["message"] = "Le mot de passe est incorrect";

                    //on créé la vue
                    $vue = new ConnexionVue($tab);
                    $vue->render(3);
                }
            }
        }catch (\Exception $e){

        }


    }

    public function afficherMenu(Container $c, Response $r){
        //on vérifie que la personne est bien connectée
        if(isset($_SESSION["id"])){
            $routeur = $c["router"];
            $vue = new ConnexionVue($routeur);
            $vue->render(4);

            return $r;
        }else{
            //on récupère le routeur
            $routeur = $c["router"];

            //on redirige vers la connexion
            $url = $routeur->pathFor("connexionRiver");
            return $r->withRedirect($url);
        }
    }

    public function deconnexion(Container $c, $rep){
        //On détruit la session
        session_destroy();

        //on redirige
        $url = $c["router"]->pathFor("connexionRiver");
        return $rep->withRedirect($url);
    }


}