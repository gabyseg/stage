
function placerHexagone(x, y, scene) {
    //let hexa = scene.physics.add.sprite(x, y, 'hexa').setInteractive({pixelPerfect : true});
    //hexa.setScale(0.2)
}

function placerHexagones(scene, taille, section) {
    //pour définir la forme
    let forme = [1, 2, 2, 2, 3, 6, 3, 6, 3, 3, 4, 3, 1, 6, 5, 5];

    //on ajoute un nouveau groupe
    var hexagones = scene.physics.add.group({
        key : "hexa",
        repeat : section.questions.length-1,
    });

    hexagones.children.iterate(function (child, index) {
        if (typeof taille === 'undefined' || taille === null) {
            child.displayWidth = taille;
            child.displayHeight = taille;
        }else{
            child.setScale(0.2);
        }
    });

    let tab = hexagones.children.entries;

   for(let i=0 ; i<tab.length ; i++){
        let hexa = tab[i];

        //si ce n'est pas le premier
        if(i !== 0){
            let hexaAvant = tab[i-1];
            let valeurs = getCoordonnees(hexaAvant, forme, i);
            hexa.x = valeurs.x;
            hexa.y = valeurs.y;

            while (enChevaucheUn(tab, hexa, hexa.x, hexa.y)){
                valeurs = getCoordonnees(hexaAvant, forme, i);
                hexa.x = valeurs.x;
                hexa.y = valeurs.y;
            }
        }else{
            hexa.x = 200;
            hexa.y = 400;
        }

        //on l'ajoute dans le tableau des hexagones de la section
       section.ajouterHexagone(hexa.x, hexa.y, hexa, section.questions[i], scene);
    }
}

function getCoordonnees(hexaAvant, tableauForme, indice) {
    //pour la réponse
    let rep = {};
    indice--;

    //on prend un nombre au hasard
    //let nb = Phaser.Math.Between(1, 6);
    var nb2 = -1;
    if(indice < tableauForme.length){
        nb2 = tableauForme[indice];
    }else{
        nb2 = Phaser.Math.Between(1, 6);
    }

    //selon celui là
    switch (nb2) {
        case 1:
            //en bas à droite
            rep.x = hexaAvant.x + hexaAvant.displayWidth/1.9;
            rep.y = hexaAvant.y + hexaAvant.displayHeight/1.3;
            break;
        case 2:
            //en haut à droite
            rep.x = hexaAvant.x + hexaAvant.displayWidth/2;
            rep.y = hexaAvant.y - hexaAvant.displayHeight/1.3;
            break;
        case 3:
            //à droite
            rep.x = hexaAvant.x + hexaAvant.displayWidth/0.97;
            rep.y = hexaAvant.y;
            break;
        case 4:
            //en haut à gauche
            rep.x = hexaAvant.x - hexaAvant.displayWidth/1.9;
            rep.y = hexaAvant.y - hexaAvant.displayHeight/1.3;
            break;
        case 5:
            //vers la gauche
            rep.x = hexaAvant.x - hexaAvant.displayWidth/0.97;
            rep.y = hexaAvant.y;
            break;
        case 6:
            //en bas à gauche
            rep.x = hexaAvant.x - hexaAvant.displayWidth/2;
            rep.y = hexaAvant.y + hexaAvant.displayHeight/1.3;
            break
    }

    return rep;
}

function enChevaucheUn(tableauHexagones, hexa, x, y) {
    let chevauche = false;

    //on parcourt tout le tableau pour voir si ça va se chevaucher
    tableauHexagones.forEach(function (element) {
        if(hexa !== element){
            if(element.x+element.displayWidth < x || element.x - element.displayWidth > x){

            }else if(element.y+element.displayHeight < y || element.y - element.displayHeight > y){

            }else if (element.x+element.displayWidth/2 <= x && ( element.y -element.displayHeight/2 >= y  || element.y + element.displayHeight/2 <= y)){
                //si il est en haut à droite ou en bas à droite
            }else if (element.x-element.displayWidth/2 >= x && ( element.y -element.displayHeight/2 >= y  || element.y + element.displayHeight/2 <= y)){
                //si il est en haut à gauche ou en bas à gauche
            }else{
                chevauche = true;
            }
        }
    });

    return chevauche;
}



export default {
    placerHexagone : placerHexagone,
    placerHexagones : placerHexagones,
}