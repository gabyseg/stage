import Section from "./Section.js";
import loader from "../../../../../js/requetes/loader.js";
import sc from "../SceneHexagones.js";
import SceneBoutons from "../SceneBoutons.js";
import SceneHexagones from "../SceneHexagones.js";
import ScrollerPlugin from "../../../plugins/scroller-plugin.js";
import tableau from "./TableauDeCouleurs.js";

class JeuDonnees {
    constructor() {
        let jeuDonnes = this;

        //ici on va choper les resultats via axios et api
        loader.init(window.location.origin);
        loader.charger("/river/getRepresentationVisuelle").then(function (reponse) {
            let id = reponse.data;

            loader.charger("/api/representation/"+id).then(function (reponse) {
                jeuDonnes.tabNbSections = reponse.data.sections;


                //le tableau de ses sections
                var sections = [];

                //pour chaque section
                jeuDonnes.tabNbSections.forEach(function (elem) {
                    var s = new Section(elem);
                    sections.push(s);
                });

                jeuDonnes.sections = sections;


                var config = {
                    type: Phaser.AUTO,
                    parent : document.getElementById("jeu"),
                    physics: {
                        default: 'arcade'
                    },
                    scene : [SceneBoutons, SceneHexagones],
                    dom: {
                        createContainer: true,
                    },
                    width : $("#jeu").width(),
                    height : 650,
                    plugins: {
                        global: [{
                            key: 'rexScroller',
                            plugin: ScrollerPlugin,
                            start: true
                        }]
                    }
                };

                //pour la légende ici
                reponse.data.elements.forEach(function (elem) {
                    var nom = elem.nomMethode;
                    nom = nom.replace(" ", "");
                    let couleur = tableau.ajouterElement(nom).replace("0x", "#");
                    $("#" + nom).css("background-color", couleur);
                    console.log($("#" + nom));
                });

                var game = new Phaser.Game(config);

                tableau.afficher();
            });
        });


        //en attendant on simule 3 sections de tailles différentes
       /* this.tabNbSections = [
            {questions : [{question : "comment tu vas ?", variable : "gentillesse"}, {question : "et toi?", variable: "politesse"}, {question : "super non?", variable : "accord"}], section : "nouvelles"},
            {questions : [{question : "Ça t'as coûté combien?", variable : "curiosité"}, {question : "non ?", variable : "doute"}, {question : "quoi que?", variable : "doute"}, {question : "autant ?", variable : "certain"}], section : "argent"},


        ];*/


    }

    placerLesSections(cam){
        let sections = this.sections;

        this.sections.forEach(function (elem, index) {
            //l'écart
            let ecart = 2/(sections.length);
            //la position de la section
            let x = cam.centerX + (Math.cos(Math.PI*index*ecart)*cam.displayWidth/1.6);
            let y = cam.centerY + (Math.sin(Math.PI*index*ecart)*cam.displayHeight/1.6);

            elem.changerCoordonnees(x, y);
        })
    }
}

//on créé le jeu
let jeu = new JeuDonnees();

export default jeu;