import tableauCouleurs from "./TableauDeCouleurs.js";

class Hexagone{
    constructor(x, y, hexa, question, scene){
        this.x = x;
        this.y = y;
        this.figure = hexa;
        this.question = question.intitule;
        this.scores = question.scores;
        this.scene = scene;
        this.grenouilles = [];
        this.serpents = [];
        this.noteMax = question.noteMax;
    }

    bouger(dx, dy){
        this.x += dx;
        this.y += dy;

        this.figure.x = this.x;
        this.figure.y = this.y;
        //si un des éléments a une note trop faible

        let valeurs = this.determinerVal1Val2(this.scores);
        if(valeurs.grande.valeur < this.noteMax/2){
            this.placerSerpent(this.scene, valeurs.grande.nom);
        }else{
            this.determinerGrenouilles(valeurs, this.figure, this.scene)
        }
    }

    determinerGrenouilles(valeurs, hexa, scene){
        if(valeurs.grande.valeur > valeurs.moyenne.valeur*1.2){
            var gre = scene.physics.add.sprite(this.x, this.y, 'grenouille').setScale(0.09);
            var gre1 = scene.physics.add.sprite(this.x, this.y, 'grenouille').setScale(0.09);
            gre1.setTintFill(Phaser.Display.Color.HexStringToColor(tableauCouleurs.ajouterElement(valeurs.grande.nom.replace(" ", ""))).color);
            gre1.setAlpha(0.7);

            //on en garde une trace
            this.grenouilles.push(gre1);
            this.grenouilles.push(gre);
        }else{
            var g2 = scene.physics.add.sprite(this.x, this.y, 'grenouille').setScale(0.09);
            var g3 = scene.physics.add.sprite(this.x, this.y, 'grenouille').setScale(0.09);
            g3.setTintFill(Phaser.Display.Color.HexStringToColor(tableauCouleurs.ajouterElement(valeurs.moyenne.nom.replace(" ", ""))).color);
            g3.setAlpha(0.7);

            var g1 = scene.physics.add.sprite(this.x-10, this.y-10, 'grenouille').setScale(0.09);
            var g = scene.physics.add.sprite(this.x-10, this.y-10, 'grenouille').setScale(0.09);
            g.setTintFill(Phaser.Display.Color.HexStringToColor(tableauCouleurs.ajouterElement(valeurs.grande.nom.replace(" ", ""))).color);
            g.setAlpha(0.7);

            //on en garde une trace
            this.grenouilles.push(g1);
            this.grenouilles.push(g);

            this.grenouilles.push(g2);
            this.grenouilles.push(g3);

        }
    }

    placerSerpent(scene, nom){
        var s1 = scene.physics.add.sprite(this.x, this.y, 'serpent').setScale(0.5);
        var s = scene.physics.add.sprite(this.x, this.y, 'serpent').setScale(0.5);
        s.setTintFill(Phaser.Display.Color.HexStringToColor(tableauCouleurs.ajouterElement(nom.replace(" ", ""))).color);
        s.setAlpha(0.7);

        this.serpents.push(s1);
        this.serpents.push(s);
    }

    cacher(){
        this.grenouilles.forEach(function (grenouille) {
            grenouille.disableBody(true, true);
        });
        this.serpents.forEach(function (serpent) {
            serpent.disableBody(true, true);
        });
    }

    montrer(){
        let ceci = this;
        var compteur = 0;

        this.grenouilles.forEach(function (grenouille) {
            if(compteur < 2){
                grenouille.enableBody(true,ceci.x, ceci.y, true, true);
            }else{
                grenouille.enableBody(true,ceci.x-10, ceci.y-10, true, true);
            }
            compteur++;
        });

        this.serpents.forEach(function (serpent) {
            serpent.enableBody(true,ceci.x-10, ceci.y-10, true, true);
        });
    }

    determinerVal1Val2(tableauScores){
        let grande = {nom : "", valeur : -1};
        let moyenne = {nom : "", valeur : -1};

        Object.keys(tableauScores).forEach(function (cle) {
            let val = tableauScores[cle];

            if(val > grande.valeur){
                grande.valeur = val;
                grande.nom = cle;
            }else if(val<=grande.valeur && val > moyenne.valeur){
                moyenne.valeur = val;
                moyenne.nom = cle;
            }
        });

        return {grande : grande, moyenne : moyenne};
    }
}

export default Hexagone;