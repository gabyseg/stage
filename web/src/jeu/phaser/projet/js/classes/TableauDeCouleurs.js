
let tabCouleur = {};

tabCouleur.tableau = [];
tabCouleur.couleur = ["#FF0000", "#00FF00", "#FFFF00","#0000FF"];
tabCouleur.variables = ["#D98880", "#85929E", "#BB8FCE", "#2471A3", "#76D7C4", "#73C6B6","#F1948A",  "#F1C40F", "#C39BD3", "#F39C12", "#52BE80", "#D35400"];

tabCouleur.ajouterVariable = function (variable) {
    //si la variable n'a pas encore de couleur attribuée
    if(!Object.keys(tabCouleur.tableau).includes(variable)){
        //tabCouleur.tableau[variable] = '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
        //tabCouleur.tableau[variable] = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
       /* var couleur = '#'+Math.floor(Math.random()*16777215).toString(16);

        while(couleur.length !== 7){
            couleur = '#'+Math.floor(Math.random()*16777215).toString(16);
        }

        tabCouleur.tableau[variable] = couleur;*/
        tabCouleur.tableau[variable] = tabCouleur.variables[0];
        tabCouleur.variables.splice(0, 1);

    }
    //on retourne la couleur
    return tabCouleur.tableau[variable];
};

tabCouleur.ajouterElement = function(element){
    //si l'élément n'est pas encore inclu
    if(!Object.keys(tabCouleur.tableau).includes(element)){
        tabCouleur.tableau[element] = tabCouleur.couleur[0];
        tabCouleur.couleur.splice(0, 1);
    }

    return tabCouleur.tableau[element];
};

tabCouleur.afficher = function () {
  console.log(tabCouleur.tableau);
};

export default {
    ajouterVariable : tabCouleur.ajouterVariable,
    ajouterElement : tabCouleur.ajouterElement,
    afficher : tabCouleur.afficher,
};