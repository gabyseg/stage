import Hexagone from "./Hexagone.js";
import tableau from "./TableauDeCouleurs.js";

class Section {
    constructor(infos){
        this.questions = infos.questions;
        this.section = infos.intitule;
        this.hexagones = [];
        this.x = 0;
        this.y = 0;
        this.bateaux = [];
    }

    ajouterHexagone(x, y, hexa, question, scene){
        this.scene = scene;
        this.hexagones.push(new Hexagone(x, y, hexa, question, scene));

        hexa.setInteractive({pixelPerfect : true});
        //hexa.on('pointerdown',this.clicked);

        hexa.question = question.intitule;
        hexa.variable = question.variable;
        hexa.section = this.section;
        hexa.scores = question.scores;

        //pour la couleur
        let couleur = Phaser.Display.Color.HexStringToColor(tableau.ajouterVariable(hexa.variable)).color;
        hexa.setTintFill(couleur);

        //on recalcule sa taille
        this.mettreAJourTaille()
    }

    changerCoordonnees(x, y){
        //on passe les nouvelles valeurs
        this.x = x;
        this.y = y;

        //on change la position des hexagones
        this.hexagones.forEach(function (elem) {
            elem.bouger(x, y);
        });

        //on recalcule sa taille
        this.mettreAJourTaille();
        this.determinerBateau();
    }

    mettreAJourTaille(){
        let x = {max : -100000, min : 100000};
        let y = {max : -100000, min : 100000};

        //on parcourt tous les hexagones
        this.hexagones.forEach(function (element) {
            if(element.x - element.figure.width/2 < x.min) x.min = element.x - element.figure.width/2;
            if(element.x + element.figure.width/2 > x.max) x.max = element.x + element.figure.width/2;
            if(element.y - element.figure.height/2 < y.min) y.min = element.y - element.figure.height/2;
            if(element.y + element.figure.height/2 > y.max) y.max = element.y + element.figure.height/2;
        });

        this.coordonnees = {x : x, y : y};
    }

    determinerBateau(){
        let tabRes = {};

        //pour chaque question
        this.questions.forEach(function (question) {
            //pour chacun de ses résultats
            Object.keys(question.scores).forEach(function (cle) {
                if(tabRes.hasOwnProperty(cle)){
                    tabRes[cle] += question.scores[cle];
                }else{
                    tabRes[cle] = question.scores[cle];
                }

            });
        });

        //on cherche le max
        var nom = "";
        var max = -1000;
        Object.keys(tabRes).forEach(function (cle) {
            if(tabRes[cle] > max){
                max = tabRes[cle];
                nom = cle;
            }
        });

        var coeff = 0;
        if(this.coordonnees.x.min > 0){
            coeff = 0.15;
        }else{
            coeff = -1;
        }

        var b1 = this.scene.physics.add.sprite(this.coordonnees.x.min + (this.coordonnees.x.min*coeff), (this.coordonnees.y.max + this.coordonnees.y.min)/2, 'bateau').setScale(0.3);
        var b = this.scene.physics.add.sprite(this.coordonnees.x.min + (this.coordonnees.x.min*coeff), (this.coordonnees.y.max + this.coordonnees.y.min)/2, 'bateau').setScale(0.3);
        b.setTintFill(Phaser.Display.Color.HexStringToColor(tableau.ajouterElement(nom.replace(" ", ""))).color);
        b.setAlpha(0.7);

        this.bateaux.push(b1);
        this.bateaux.push(b);
    }

    cacher(){
        this.hexagones.forEach(function (elem) {
            elem.cacher();
        });

        this.bateaux.forEach(function (b) {
            b.disableBody(true, true);
        });
    }

    montrer(){
        this.hexagones.forEach(function (elem) {
            elem.montrer();
        });

        var coeff = 0;
        if(this.coordonnees.x.min > 0){
            coeff = 0.15;
        }else{
            coeff = -1;
        }

        let ceci = this;
        this.bateaux.forEach(function (b) {
            b.enableBody(true,ceci.coordonnees.x.min + (ceci.coordonnees.x.min*coeff), (ceci.coordonnees.y.max + ceci.coordonnees.y.min)/2, true, true);
        });
    }
}

export default Section;