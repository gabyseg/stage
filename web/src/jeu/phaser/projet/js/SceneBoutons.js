import constantes from "./constantes.js";
import jeu from "./classes/JeuDonnees.js";

export default class SceneBoutons extends Phaser.Scene {

    constructor(){
        super({key:"boutons", active:true})
    }

    preload(){
        //on charge l'image des boutons à venir
        this.load.image("hexa", "../../src/jeu/phaser/projet/assets/hexa.png");
    }

    create() {
        //pour le drag
        const pad = Phaser.Utils.String.Pad;
        let cam = this.cameras.main;

        var x = this.cameras.main.centerX,
            y = this.cameras.main.centerY,
            w = this.cameras.main.width,
            h = this.cameras.main.height;
        var topY = y - (h / 2),
            leftX = x - (w / 2);
        var bg = this.add.graphics()
            .setPosition(leftX, topY)
            .fillStyle(0x87CEFA, 1)
            .fillRect(0, 0, w, h)
            .setInteractive(new Phaser.Geom.Rectangle(0, 0, w, h),
                Phaser.Geom.Rectangle.Contains);



        //la caméra principal
        let cameraPrincipale = this.cameras.main;

        //couleur bleu du fond
        cameraPrincipale.setBackgroundColor('#87CEFA');

        //ici on met le titre
        var titre = document.createElement("h1");
        titre.id = "titre";
        var element = this.add.dom(cameraPrincipale.centerX, 30, titre, 'font-size: 48px; font-weight: bold;', 'Section n°');
        element.setInteractive();



        //on récupère la scène où on veut agir
        var scene = this.scene.get("hexagones");


        //drag suite
        this.scroller = this.plugins.get('rexScroller').add(bg, {
            bounds: [
                -2000, //scroll sur x
                0      //scroll sur y
            ],
            value: -cam.width/2, //position x, y, var topBound = topY, bottomBound;
            slidingDeceleration: 5000,
            backDeceleration: 2000,
            orientation : "horizontal",

            valuechangeCallback: function(newValue) {
                let camera = scene.cameras.main;
                camera.scrollX = -newValue;
            }
        });

        this.scroller2 = this.plugins.get('rexScroller').add(bg, {
            bounds: [
                0,          //scroll sur x
                -2000       //scroll sur y
            ],
            value: -cam.height/2, //position x, y, var topBound = topY, bottomBound;
            slidingDeceleration: 5000,
            backDeceleration: 2000,
            orientation : "vertical",

            valuechangeCallback: function(newValue) {
                let camera = scene.cameras.main;
                camera.scrollY = -newValue;
            }
        });

        scene.reCentrer();

        //pour les boutons
        this.ajouterBoutons();
        this.ajouterActions();

    }

    ajouterBoutons(){
        let cameraPrincipale = this.cameras.main;

        //on ajoute le bouton vers la section précédente
        var boutonPrecedent = this.add.sprite(0, 0, "hexa").setInteractive({pixelPerfect : true});
        boutonPrecedent.setScale(0.15);
        boutonPrecedent.x = boutonPrecedent.x + boutonPrecedent.displayWidth;
        boutonPrecedent.y = boutonPrecedent.y + boutonPrecedent.displayHeight/1.8;
        //la couleur
        boutonPrecedent.setTintFill(0xDC143C);


        //on ajoute le bouton vers la section suivante
        var boutonSuivant = this.add.sprite(cameraPrincipale.displayWidth, 0, "hexa").setInteractive({pixelPerfect : true});
        boutonSuivant.setScale(0.15);
        boutonSuivant.x = boutonSuivant.x - boutonSuivant.displayWidth;
        boutonSuivant.y = boutonSuivant.y + boutonSuivant.displayHeight/1.8;
        //la couleur
        boutonSuivant.setTintFill(0x32CD32);

        //les boutons qu'on garde
        this.boutons = {precedent : boutonPrecedent, suivant : boutonSuivant};


        //flèche de gauche
        var elem = document.createElement("span");
        elem.className = "oi oi-arrow-circle-left";
        elem.style = "font-size : 40px";

        var flecheGauche = this.add.dom(boutonPrecedent.x, boutonPrecedent.y, elem);

        //flèche de droite
        var elem2 = document.createElement("span");
        elem2.className = "oi oi-arrow-circle-right";
        elem2.style = "font-size : 40px";

        var flecheDroite = this.add.dom(boutonSuivant.x, boutonSuivant.y, elem2);
    }

    ajouterActions(){
        //on récupère les boutons
        var boutonSuivant = this.boutons.suivant;
        var boutonPrecedent = this.boutons.precedent;

        var scene = this.scene.get("hexagones");
        this.indexCourant = -1;

        //pour les actions
        var indexCourant = this.indexCourant;

        let sceneActuelle = this;

        //les listener
        boutonPrecedent.on("pointerup", function () {
            //on va faire bouger la caméra ici
            let camera = scene.cameras.main;

            //on vérifie qu'il y a toujours une section après
            if(indexCourant-1 >= 0){
                indexCourant--;

                //on récupère la section
                let section = jeu.sections[indexCourant];
                camera.zoomTo(1, 1000);

                //on cache les autres sections
                sceneActuelle.modifierApparence(false, section);

                //on bouge la caméra
                camera.pan((section.coordonnees.x.max + section.coordonnees.x.min)/2, (section.coordonnees.y.max + section.coordonnees.y.min)/2, 400);

                //on affiche le nom de la section
                $("#section").html("Section : " + section.section);
                $("#titre").html("Section n° " + (indexCourant+1));
            }else{
                indexCourant = -1;

                //on re positionne la caméra
                scene.reCentrer();
                //on remet à 0 le nom de la section
                $("#section").html("Section : ");
                $("#titre").html("Section n°");

                sceneActuelle.modifierApparence(true);
            }
        });

        boutonSuivant.on("pointerup", function () {
            //on va faire bouger la caméra ici
            let camera = scene.cameras.main;
//alert();
            //on vérifie qu'il y a toujours une section après
            if(indexCourant+1 < jeu.sections.length){
                indexCourant++;
                //on récupère la section
                let section = jeu.sections[indexCourant];
                camera.zoomTo(1, 1000);
                console.log(section);

                //on cache les autres sections
                sceneActuelle.modifierApparence(false, section);

                //on bouge la caméra
                camera.pan((section.coordonnees.x.max + section.coordonnees.x.min)/2, (section.coordonnees.y.max + section.coordonnees.y.min)/2, 400);

                //on affiche le nom de la section
                $("#section").html("Section : " + section.section);
                $("#titre").html("Section n° " + (indexCourant+1));
            }else{
                indexCourant = -1;

                //on re positionne la caméra
                scene.reCentrer();

                sceneActuelle.modifierApparence(true);
                //on remet à 0 le nom de la section
                $("#section").html("Section : ");
                $("#titre").html("Section n°");
            }

        });
    }

    modifierApparence(montrer){
        //selon le non d'arguments
        if(arguments.length === 2) {
            //on récupère la section
            let section = arguments[1];
            //on les parcours toutes
            jeu.sections.forEach(function (s) {
                if(section !== s) {
                    s.hexagones.forEach(function (elem) {
                        elem.figure.disableBody(true, true);
                    });
                    s.cacher();
                }else{
                    section.hexagones.forEach(function (elem) {
                        elem.figure.enableBody(true, elem.x, elem.y, true ,true);
                    });
                    s.montrer();
                }
            })
        }else{
            //on les récative tous
            jeu.sections.forEach(function (s) {
                s.hexagones.forEach(function (elem) {
                    elem.figure.enableBody(true, elem.x, elem.y, true ,true);
                });
                s.montrer();
            })
        }
    }
}