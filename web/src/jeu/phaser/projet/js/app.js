import SceneBoutons from "./SceneBoutons.js";
import SceneHexagones from "./SceneHexagones.js";
import ScrollerPlugin from '../../plugins/scroller-plugin.js';
import jeu from "./classes/JeuDonnees.js";
import loader from "../../../../js/requetes/loader.js";
import tableau from "./classes/TableauDeCouleurs.js";

//quand le document est chargé
$(document).ready(function () {
    loader.init(window.location.origin);

    //on charge les graphiques
    loader.charger("/api/graphiques").then(function (reponse) {
        //on récupère les données
        let donnees = reponse.data;
        console.log(donnees);
        //pour chacune des réponses on génère son graphique
        donnees.forEach(function (elem) {
            let $canva = $("<canvas class='mb-5'></canvas>");
            $("#ensembleGraphiques").append($canva);

            //on ajoute la couleur
            elem.elements.datasets.forEach(function (e) {
                e.backgroundColor = [tableau.ajouterElement(e.label.replace(" ", ""))];
            });

            if(elem.elements.labels[0].length >= 130){
                elem.elements.labels[0] = elem.elements.labels[0].substring(0, 130) + "...";
            }

            //on initialise les graphiques
            var myContext = $canva;
            var myChartConfig = {
                type: 'bar',
                data: elem.elements,
                responsive : true,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                max : elem.noteMax,
                            }
                        }]
                    }
                }
            };
            var myChart = new Chart(myContext, myChartConfig);
        });


    });

});