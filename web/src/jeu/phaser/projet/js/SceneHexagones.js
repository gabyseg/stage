import hexagonePlaceur from "./hexagonePlaceur.js";
import jeu from "./classes/JeuDonnees.js";

export default class SceneHexagones extends Phaser.Scene {

    constructor(){
        super({key:"hexagones", active:true})
    }

    preload(){
        //on charge l'image des boutons à venir
        this.load.image("hexa", "../../src/jeu/phaser/projet/assets/hexa.png");
        this.load.image("grenouille", "../../src/jeu/phaser/projet/assets/grenouille.png");
        this.load.image("bateau", "../../src/jeu/phaser/projet/assets/bateau.png");
        this.load.image("serpent", "../../src/jeu/phaser/projet/assets/serpent.png");
    }

    create() {
        /*var hexa = this.physics.add.sprite(300, 300, 'hexa').setInteractive({pixelPerfect : true});
        hexa.setScale(0.2);

        hexa.on('pointerover', function () {
            //pour la couleur
            this.setTintFill(0xff0000);
        });

        hexa.on('pointerout', function () {

            this.setTintFill();
        });

        //quand on clique dessus
        hexa.on('pointerdown', function (pointer) {
            //this.scene.launch("vue", {});
            if(pointer.leftButtonDown()){
                this.cameras.main.zoom = this.cameras.main.zoom + 0.1;
            }else if(pointer.rightButtonDown()){
                this.cameras.main.zoom = this.cameras.main.zoom - 0.1;
            }

        }, this);*/


        //pour la taille d'un sprite
        //console.log(hexa.displayWidth);
       /* let hexa2 = this.physics.add.sprite(hexa.x + hexa.displayWidth/2, hexa.y + hexa.displayHeight/1.3 , 'hexa').setInteractive({pixelPerfect : true}).setScale(0.2).setInteractive({pixelPerfect : true});

        // hexa.tint = Math.random() * 0xffffff;

        hexa2.on('pointerdown', function (pointer, localX, localY, event) {
            //pour la couleur
            this.setTintFill(0xff0000);
            console.log("moi");
        });*/






        //let hexa3 = this.physics.add.sprite(this.cameras.main.displayWidth+1500, 300 , 'hexa').setInteractive({pixelPerfect : true}).setScale(0.2).setInteractive({pixelPerfect : true});
        //hexagonePlaceur.placerHexagone(700, 300, this);

        //on initialise les données du jeu
        let scene = this;

        //on parcourt les sections
        jeu.sections.forEach(function (elem) {
            hexagonePlaceur.placerHexagones(scene, 300, elem);
        });

        jeu.placerLesSections(this.cameras.main);


        this.cameras.main.zoom = 0.5;


        //ici on effectue le traitement des hexagones
        this.input.on('gameobjectdown', function (pointer, gameObject) {
            if(gameObject.hasOwnProperty("question") && gameObject.hasOwnProperty("variable") && gameObject.hasOwnProperty("scores")){
                //on ajoute dans les informations
                $("#question").html("Question : " + gameObject.question);
                $("#variable").html("Variable : " + gameObject.variable);
                $("#section").html("Section : " + gameObject.section);

                //on ajoute les scores
                $("#scores").empty();
                console.log(gameObject.scores);
                Object.keys(gameObject.scores).forEach(function (element) {
                    let $li = $("<li></li>");
                    $li.html(element + " : " + gameObject.scores[element]);
                    $("#scores").append($li);
                })
            }
        });


        //grenouille
      /*  var g1 = this.physics.add.sprite(300, 800, 'grenouille').setScale(0.1);
        var g = this.physics.add.sprite(300, 800, 'grenouille').setScale(0.1);
        g.setTintFill("0xff0000");
        g.setAlpha(0.4);

        var g2 = this.physics.add.sprite(300, 790, 'grenouille').setScale(0.1);
        var g3 = this.physics.add.sprite(300, 790, 'grenouille').setScale(0.1);
        g3.setTintFill("0x0000ff");
        g3.setAlpha(0.4);

        //bateau
        var b1 = this.physics.add.sprite(1000, 800, 'bateau').setScale(0.3);
        var b = this.physics.add.sprite(1000, 800, 'bateau').setScale(0.3);
        b.setTintFill("0xff0000");
        b.setAlpha(0.4);

        //serpent
        var s1 = this.physics.add.sprite(1000, 1000, 'serpent').setScale(0.4);
        var s = this.physics.add.sprite(1000, 1000, 'serpent').setScale(0.4);
        s.setTintFill("0xff0000");
        s.setAlpha(0.4);*/
    }

    reCentrer(){
        let cam = this.cameras.main;
        cam.zoomTo(0.5, 1000);
        cam.setScroll(cam.width/3, cam.height/1.5);
        //cam.zoom=0.5;
    }

}