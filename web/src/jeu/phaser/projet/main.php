<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Jeu</title>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/phaser@3.17.0/dist/phaser.js"></script>
        <script src="../../../bootstrap/js/bootstrap.js"></script>
        <link href="../../../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="../../../open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../../../bootstrap/css/questionnaire.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

    </head>
    <body>

        <?php
           // if (isset($_SESSION["afficherReprésentation"])){

                //on sélectionne le questionnaire
                $q = \test\modele\Questionnaire::find($_SESSION["afficherReprésentation"]);

                $elements = "";
                foreach ($q->elementsCompares as $elem){
                    $nom = $elem->nomMethode;
                    $remp = str_replace(" ", "", $nom);
                    $res = <<<END
                    <div><p><img class="couleurElement mr-2" id="$remp"/>$nom</p></div>
END;
                    $elements .= $res;
                }

                echo <<<END
            <div class="container">
                <div class="row mt-4">
                    <div id="jeu" class="col-lg-9">
    
    
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header text-center" id="infosHeader">
                                <h5>Informations</h5>
                            </div>
                            <div class="card-body">
                                <p id="section">Section : </p>
                                <hr>
                                <p id="question">Question : </p>
                                <hr>
                                <p id="variable">Variable : </p>
                                <hr>
                                <p id="">Scores : </p>
                                <ul id="scores">
    
                                </ul>
                            </div>
                        </div>
                        <button class="btn btn-danger mt-2" data-toggle="modal" data-target="#diagrammes">
                            Voir les graphiques
                        </button>
                    </div>
                </div>
                
                <div class="row mt-4" id="legende">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header text-center" id="infosHeader">
                                <h5>Légende</h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md" id="elementsDeLegende">
                                        <p><img src="../../img/hexa.png" class=""> Nénuphar, il représente une question ou un groupement</p>
                                        <p><img src="../../img/grenouille.png" class=""> La ou les techniques ayant obtenu le meilleur score</p>
                                        <p><img src="../../img/bateau.png" class=""> Technique ayant le meilleur score sur cet îlot</p>
                                        <p><img src="../../img/serpent.png" class=""> Technique ayant eu une note nettement plus faible que les autres</p>
                                    </div>
                                    <div class="col-md" id="codesCouleurs">
                                        $elements
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
END;
           // }


        ?>


        <!-- Fenêtre modale -->
        <div class="modal fade" id="diagrammes" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog-centered modal-dialog modal-lg" role="document">
                <div class="modal-content bordureRouge">

                    <div class="modal-header">
                        <div class="text-center col-md">
                            <h4 class="modal-title">Les diagrammes</h4>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body" id="ensembleGraphiques">

                    </div>
                </div>
            </div>
        </div>

        <script type="module" src="../../src/jeu/phaser/projet/js/app.js"></script>
    </body>
</html>