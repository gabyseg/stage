<?php
    //on créé la personne directement
    $personne = new \test\modele\Personne();
    $personne->save();

    //le tableau
    $tabDonnes = ["idPersonne" => $personne->idPersonne];

    //la session
    $_SESSION["idPersonne"] = $personne->idPersonne;
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Questionnaire</title>
    <link href="../../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../../open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
    <link href="../../bootstrap/css/questionnaire.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/phaser@3.17.0/dist/phaser.js"></script>
</head>

<body>
<div class="container col-md-8">
    <div class="row mb-2">
        <div class="col-md-12">
            <h2 class="text-center">Programme Mirabelle+ de l'Université de Lorraine</h2>
        </div>
    </div>


    <div class="row bordureTitre mb-2">
        <p id ="titre" class="titre">TITRE </p>
    </div>

    <div class="row" id="questions">
     <!--   <div id="q1" data-type="multiple">
            <p>Question bidon</p>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Très mauvaise</th>
                        <th scope="col">Plutôt mauvaise</th>
                        <th scope="col">Moyennement bonne</th>
                        <th scope="col">Plutôt bonne</th>
                        <th scope="col">Très bonne</th>
                        <th scope="col">NSP</th>
                        <th scope="col">NA</th>
                    </tr>
                </thead>
                <tbody class="text-center">

                <tr class="fond1">
                    <th scope="row">TEP</th>
                    <td><input type="radio" name="TEP"></td>
                    <td><input type="radio" name="TEP"></td>
                    <td><input type="radio" name="TEP"></td>
                    <td><input type="radio" name="TEP"></td>
                    <td><input type="radio" name="TEP"></td>
                    <td><input type="radio" name="TEP"></td>
                    <td><input type="radio" name="TEP"></td>
                </tr>
                <tr class="fond2">
                    <th scope="row">IRM</th>
                    <td><input type="radio" name="IRM"></td>
                    <td><input type="radio" name="IRM"></td>
                    <td><input type="radio" name="IRM"></td>
                    <td><input type="radio" name="IRM"></td>
                    <td><input type="radio" name="IRM"></td>
                    <td><input type="radio" name="IRM"></td>
                    <td><input type="radio" name="IRM"></td>
                </tr>
                <tr class="fond1">
                    <th scope="row">Scanner</th>
                    <td><input type="radio" name="Scanner"></td>
                    <td><input type="radio" name="Scanner"></td>
                    <td><input type="radio" name="Scanner"></td>
                    <td><input type="radio" name="Scanner"></td>
                    <td><input type="radio" name="Scanner"></td>
                    <td><input type="radio" name="Scanner"></td>
                    <td><input type="radio" name="Scanner"></td>
                </tr>
                </tbody>
            </table>
        </div>-->
    </div>

    <div>
        <button id="boutonSuivant" class="btn-danger btn">Suivant</button>
        <p id="messageErreur">Un ou plusieurs champs n'ont pas été complétés <span class="oi oi-circle-x"></span></p>
    </div>

</div>
<script src="../../node_modules/axios/dist/axios.js"></script>
<script src="../../src/js/app.js" type="module"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</body>
</html>