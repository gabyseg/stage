<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Questionnaire</title>
    </head>
    <link href="../../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../../bootstrap/css/questionnaire.css" rel="stylesheet">
    <body>

        <div class="container col-md-8">
            <div class="row mb-2">
                <div class="col-md-12">
                    <h2 class="text-center">Programme Mirabelle+ de l'Université de Lorraine</h2>
                </div>
            </div>
            <div class="row bordureTitre mb-2">
                <p class="titre"> Projet double effect </p>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <p>Dans le cadre d’un projet financé par le programme Mirabelle+ (Lorraine Université d'Excellence),
                        qui rassemble les laboratoires CRAN, LRGP, LCPM, PTICB, IADI, CREM et Nancyclotep,
                        nous nous intéressons au positionnement de la Tomographie par émission de positons (TEP ou PET-scan)
                        face à d’autres techniques de détection.
                        Pour ce faire, nous nous sommes concentrés sur la détection des métastases cérébrales.</p>
                    <p class="mb-4">Ainsi, nous souhaitons comparer trois techniques de détection : la TEP, l’IRM, le scanner.</p>
                    <p class="mb-0">Nous avons élaboré un questionnaire qui comporte 5 étapes :</p>
                    <ul>
                        <li>Prendre connaissance de ces 3 méthodes et être convaincu</li>
                        <li>Se former et disposer de ces méthodes</li>
                        <li>Utiliser et traiter les images</li>
                        <li>Diagnostiquer suite à l’examen</li>
                        <li>Entretenir et remplacer le dispositif médical</li>
                    </ul>
                    <p class="mt-4">Toutes les questions appellent à une notation (même si votre réponse est NSP pour "Ne Sais Pas" ou NA pour "Non Applicable").</p>
                    <p>Pourriez vous prendre 10 minutes de votre temps pour remplir ce questionnaire?</p>
                    <p>Nous vous remercions par avance,</p>
                    <p>Bien cordialement,</p>
                    <p>Céline Frochot et Stéphane Goria</p>
                </div>
                <div class="col-md-3">
                    <img class="img-fluid" src="../../img/logo_doubleEffect.png">
                </div>
            </div>
            <div>
                <a href="/api/questionnaireActif" class="btn-danger btn">Démarrer</a>
            </div>
        </div>

        <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>-->
        <script src="../../node_modules/axios/dist/axios.js"></script>
    </body>
</html>