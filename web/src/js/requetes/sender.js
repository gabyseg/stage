import catchAjax from "./catchAjax.js";

var sender = {};

sender.init = function (adresse) {
    sender.urlServeur = adresse;
};

sender.envoyer = function (uri, donnees) {
    var url = sender.urlServeur + uri;
    console.log(url);

    //les paramètres de la requête
    let params = new URLSearchParams();
    params.append("donnees", JSON.stringify(donnees));
    //on créé la requête
    let promesse = axios.post(url, params);

    return promesse.catch(catchAjax);
};

sender.supprimer = function (uri) {
    var url = sender.urlServeur + uri;

    //on créé la requête
    let promesse = axios.delete(url);

    return promesse.catch(catchAjax);
};

export default {
    init : sender.init,
    envoyer : sender.envoyer,
    supprimer : sender.supprimer
}