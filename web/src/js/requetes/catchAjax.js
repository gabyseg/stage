var catchAjax = {};

catchAjax.catch = function (error) {
    if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
    } else if (error.request){
        console.log(error.request);
    } else {
        console.log("Erreur : " + error.message);
    }
};

export default {
    catchAjax : catchAjax.catch
}