import catchAjax from "./catchAjax.js";

var loader = {};

loader.init = function (adresse) {
    loader.urlServeur = adresse;
};

loader.charger = function (uri) {
    var url = loader.urlServeur + uri;
    console.log(url);
    //on récupère la promesse
    var promesse = axios.get(url, {
        "responseType" : "json",
        "withCredentials" : false
    });

    return promesse.catch(catchAjax);
};

export default {
    init : loader.init,
    charger : loader.charger
}