import questionnaire from "./questionnaire.js";
import sender from "./requetes/sender.js";

let save = {};

save.save = function (questions, sectionIntitule) {

    //selon le type de section
    switch (sectionIntitule) {
        case "Entretien":
            save.entretien();
            break;
        case "Résultats":
            save.resultat();
            break;
        case "Contact":
            save.contact("Nom");
            save.contact("Prénom");
            save.mail();
            break;
        case "Profession":
            save.sectionProfession(questions);
            break;
        default:
            //par défaut pour chaque question
            questions.forEach(function (q) {
                save.comparaison(q);
            });
            break;
    }


    /*
        //pour chaque question
        questions.forEach(function (q) {
        //selon son type
        if(q.type.nom.includes("Comparaison")){
            //on sauvegarde la réponse
            save.comparaison(q);
        }else{
            switch (q.type.nom) {
                case "Profession":
                    save.profession(q);
                    break;
                case "Spécialité":
                    save.specialite(q);
                    break;
                case "Résultat":
                    save.resultat(q);
                    break;
                case "Entretiens":
                    save.entretien(q);
                    break;
                case "Contact":
                    save.contact(q);
                    break;
                case "Mail":
                    save.mail(q);
                    break;
            }
        }
    });*/
};

//pour les comparaisons
save.comparaison = function (q) {
    //on sélectionne la div
    var $div = $("#q"+q.question.idQuestion);
    //on sélectionne les boutons cochés
    var $boutons = $div.find("input:checked");

    let reponses = [];

    //on va construire les données ici
    $boutons.each(function (index, $elem) {
        let rep = {nomMethode : $elem["name"], reponse : $elem["value"] };
        reponses.push(rep);
    });

    let donnees = {};
    donnees.idQuestion = q.question.idQuestion;
    donnees.idQuestionnaire = questionnaire.id();
    donnees["reponses"] = reponses;

    //on envoi les données
    let promesse = sender.envoyer("/api/save/question/comparaison", donnees).then(function (reponse) {
        //on ne fait rien en fait
        console.log(reponse);
    });
};

//pour la section profession
save.sectionProfession = function(questions){
    if(questions.length === 2){
        //on sauvegarde la profession
        save.profession(questions[0]);
        //on sauvegarde la spécialité
        save.specialite(questions[1]);
    }
};

//pour la Profession
save.profession = function (q) {
    //on sélectionne la div
    var $div = $("#q"+q.question.idQuestion).first();
    //on sélectionne le bouton coché
    var $bouton = $div.find("input:checked");

    //on construit la réponse
    let profession = $bouton.attr("value");

    //on envoi les données
    let promesse = sender.envoyer("/api/save/question/profession", profession).then(function (reponse) {
        //on ne fait rien en fait
        console.log(reponse);
    });
};

//pour la specialite
save.specialite = function (q) {
    //on sélectionne la div
    var $div = $("#q"+q.question.idQuestion).first();
    //on sélectionne le bouton coché
    var $bouton = $div.find("input:checked");

    //on construit la réponse
    let spe = $bouton.attr("value");

    //on envoi les données
    let promesse = sender.envoyer("/api/save/question/specialite", spe).then(function (reponse) {
        //on ne fait rien en fait
        console.log(reponse);
    });
};

//pour les résultats
save.resultat = function (q) {
    //on sélectionne le bouton coché
    var $bouton = $("input:checked");

    //on construit la réponse
    let res = $bouton.attr("value");

    //on envoi les données
    let promesse = sender.envoyer("/api/save/question/resultat", res).then(function (reponse) {
        //on ne fait rien en fait
        console.log(reponse);
    });
};

//pour les entretiens
save.entretien = function () {
    //on sélectionne le bouton coché
    var $bouton = $("input:checked");

    //on construit la réponse
    let res = $bouton.attr("value");

    //on envoi les données
    let promesse = sender.envoyer("/api/save/question/entretien", res).then(function (reponse) {
        //on ne fait rien en fait
        console.log(reponse);
    });
};

//pour les contacts
save.contact = function (id) {
    //on sélectionne le bouton coché
    var $input = $("#"+id);

    console.log($input.val());

    var nom ="";
    if(id === "Nom") nom = "nom";
    else if(id === "Prénom") nom = "prenom";


    //on construit la réponse
    let res = {valeur : $input.val(), nom : nom};

    //on envoi les données
    let promesse = sender.envoyer("/api/save/question/contact", res).then(function (reponse) {
        //on ne fait rien en fait
        console.log(reponse);
    });
};

//pour les mails
save.mail = function () {
    //on sélectionne la div
    var $input = $("#Email").first();

    //on construit la réponse
    let res = $input.val();

    //on envoi les données
    let promesse = sender.envoyer("/api/save/question/mail", res).then(function (reponse) {
        //on ne fait rien en fait
        console.log(reponse);
    });
};

export default {
    save : save.save,
}