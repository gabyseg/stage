import loader from "./requetes/loader.js";
import inserter from "./affichage/questionInserter.js"
import verifier from "./affichage/verifier.js";
import save from "./save.js";
import questionnaire from "./questionnaire.js";

var section = {};

//on initialise la section grâce à une url
section.init = function (url) {
    section.courant = url;

    //on va chercher ses informations
    var promesse = loader.charger(url);
    promesse.then(section.traiter);

    //pour savoir si on demande les entretiens
    section.entretienDemande = false;
    section.resultatDemande = false;
    section.contactAffiche = false;
};

section.traiter = function (reponse) {
    //on met à jour les données de la question
    section.section = reponse.data.section;
    section.questions = reponse.data.questions;

    section.prev = reponse.data.links.prev;
    section.next = reponse.data.links.next;
    section.q = [];

    //on change le titre
    $("#titre").html(section.section.intitule);

    //on vide le dom des questions
    $("#questions").empty();


    //pour chaque question
    section.questions.forEach(function (question) {
        section.insererQuestion(question);
    });
};

section.insererQuestion = function (question) {
    //on récupère toute la question
    var promesse = loader.charger(question.links.details).then(function (reponse) {
        let q = reponse.data;
        section.q.push(q);

        //on créé la div
        let $elemQuestion = $("<div class='col-md-12'></div>")
            .attr("id", "q" + q.question.idQuestion);

        //on créé le titre
        let $titre = $("<p></p>").html(q.question.intitule);
        $elemQuestion.append($titre);

        //on regarde selon le nom de la section
        switch (section.section.intitule) {
            case "Entretien":
            case "Résultats":
            case "Profession":
            case "Contact":
                inserter.insererQuestionCase($elemQuestion, q, "");
                break;
            default:
                inserter.insererQuestionComparaison($elemQuestion, q);
                break;
        }

        //on regarde le type de réponse
       /* if(q.type.nom.includes("Comparaison")){
            //on insère la réponse
            inserter.insererQuestionComparaison($elemQuestion, q);
        }else{
            switch (q.type.nom) {
                case "Profession":
                case "Spécialité":
                case "Résultat":
                case "Entretiens":
                    inserter.insererQuestionCase($elemQuestion, q, q.type.nom);
                    break;
                case "Contact":
                    inserter.insererQuestionContact($elemQuestion);
                    break;
                case "Mail":
                    inserter.insererQuestionMail($elemQuestion);
                    break;
            }
        }*/
    });
};

//pour changer de section
section.suivante = function () {
    var nbChampsIncorrects = verifier.verifier();
    if(nbChampsIncorrects === 0){
        //on sauvegarde les réponses
        save.save(section.q, section.section.intitule);

        if(section.next !== ""){
            //on initialise avec le lien d'après
            section.init(section.next);
        }else{
            //on coupe le listener
            $("#boutonSuivant").unbind("click", section.suivante);
            //si on a tout fait
            questionnaire.infosSupplementaires();
        }
    }
};

export default {
    init : section.init,
    suivante : section.suivante,
}