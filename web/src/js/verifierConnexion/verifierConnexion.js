$(document).ready(function () {
    //on sélectionne les champs de mots de passe
    let $mdps = $('input[type="password"]');

    //quand le formulaire est validé
    $("form").submit(function (event) {
        //on vérifie qu'on en a bien deux
        if($mdps.length === 2){
            //on vérifie qu'ils ont une valeur
            if($mdps[0].value !== "" && $mdps[1].value !== ""){
                //on vérifie qu'il ont la même valeur
                if($mdps[0].value !== $mdps[1].value){
                    //on ajoute le message d'erreur
                    $("#messageErreur").html("Les mots de passe ne correspondent pas <span class=\"oi oi-circle-x\"></span>").show();
                    //on scroll en bas de la page
                    window.scrollTo(0,document.body.scrollHeight);
                    //on bloque l'évènement
                    event.preventDefault();
                }
            }
        }
    });
});