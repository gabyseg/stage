import catchAjax from "./requetes/catchAjax.js";
import loader from "./requetes/loader.js";
import section from "./section.js";
import questionInserter from "./affichage/questionInserter.js";
import save from "./save.js";
import verifier from "./affichage/verifier.js";

var questionnaire = {};

questionnaire.init = function () {
    //on va récupérer l'adresse du questionnaire actif
    var promesse = loader.charger("/api/questionnaireVoulu");
    return promesse.then(function (reponse) {
        questionnaire.url = reponse.data;
        questionnaire.enCours = "Sections";
    });
};

questionnaire.charger = function () {
    //on charge le questionnaire
    let promesse = loader.charger(questionnaire.url);
    promesse.then(function (reponse) {
        questionnaire.idQ = reponse.data.questionnaire.idQuestionnaire;
        questionnaire.infos = {resultats : reponse.data.questionnaire.resultats, entretien : reponse.data.questionnaire.entretien };
        questionnaire.nbOuiContact = 0;
        questionnaire.representation = reponse.data.representation;

        //on vérifie qu'il y a bien des sections
        if(reponse.data.sections.length !== 0){
            //on récupère l'url de la première section
            questionnaire.urlS1 = reponse.data.sections.shift().links.details;
            section.init(questionnaire.urlS1);
        }
    }).catch(catchAjax);
};

questionnaire.id = function () {
    return questionnaire.idQ;
};

questionnaire.infosSupplementaires = function () {
    $("#questions").empty();

    //si on veut l'entretien
    if(questionnaire.infos.entretien === 1){
        //on affiche la question de l'entretien
        questionInserter.insererBoolean("Voulez-vous avoir accès aux résultats de l'enquête?", "Résultats");
        questionnaire.enCours = "Entretien";
    }else if(questionnaire.infos.resultats === 1){
        questionInserter.insererBoolean("Souhaitez-vous un entretien pour partager autour de votre questionnaire et de vos réponses ?", "Entretien");
        questionnaire.enCours = "Résultats";
    }else{
        //on vide le truc et on remercies
        //on vide le dom des questions
        $("#boutonSuivant").remove();
        $("#titre").html('Fin du questionnaire');
        $("#questions").empty().append($("<h4>Merci d'avoir participé et d'avoir répondu à ce questionnaire</h4>"));
        $("#questions").append($(`
            <h4>Voulez vous voir la représentation de ce questionnaire avec RIVER?</h4>
            <a id="representation" class="btn btn-danger" name="afficherReprésentation" href="${questionnaire.representation}">Voir la représentation</a>
        `));
    }


    $("#boutonSuivant").on("click", questionnaire.sauvegarderInfosSupp);
};

questionnaire.sauvegarderInfosSupp = function () {
    var nbChampsIncorrects = verifier.verifier();
    switch (questionnaire.enCours) {
        case "Entretien":
            if(nbChampsIncorrects===0){
                if($("input:checked").val() === "Oui") questionnaire.nbOuiContact++;
                save.save(null, "Entretien");
                $("#questions").empty();
                if(questionnaire.infos.resultats ===1){
                    questionnaire.enCours = "Résultats";
                    questionInserter.insererBoolean("Souhaitez-vous un entretien pour partager autour de votre questionnaire et de vos réponses ?", "Entretien");
                }else{
                    if(questionnaire.nbOuiContact !==0){
                        questionInserter.insererSectionContact();
                        questionnaire.enCours = "Contact";
                    }else{
                        questionnaire.enCours = "Sections";
                        //on vide le truc et on remercies
                        //on vide le dom des questions
                        $("#boutonSuivant").remove();
                        $("#titre").html('Fin du questionnaire');
                        $("#questions").empty().append($("<h4>Merci d'avoir participé et d'avoir répondu à ce questionnaire</h4>"));
                        $("#questions").append($(`
                            <h4>Voulez vous voir la représentation de ce questionnaire avec RIVER?</h4>
                            <a id="representation" class="btn btn-danger" name="afficherReprésentation" href="${questionnaire.representation}">Voir la représentation</a>
                        `));
                    }
                }
            }
            break;
        case "Résultats":
            if(nbChampsIncorrects===0){
                if($("input:checked").val() === "Oui") questionnaire.nbOuiContact++;
                save.save(null, "Résultats");
                $("#questions").empty();
                if(questionnaire.nbOuiContact !==0){
                    questionInserter.insererSectionContact();
                    questionnaire.enCours = "Contact";
                }else{
                    questionnaire.enCours = "Sections";
                    //on vide le truc et on remercies
                    //on vide le dom des questions
                    $("#boutonSuivant").remove();
                    $("#titre").html('Fin du questionnaire');
                    $("#questions").empty().append($("<h4>Merci d'avoir participé et d'avoir répondu à ce questionnaire</h4>"));
                    $("#questions").append($(`
                        <h4>Voulez vous voir la représentation de ce questionnaire avec RIVER?</h4>
                        <a id="representation" class="btn btn-danger" name="afficherReprésentation" href="${questionnaire.representation}">Voir la représentation</a>
                    `));
                }
            }
            break;
        case "Contact":
            if(nbChampsIncorrects===0){
                save.save(null, "Contact");
                questionnaire.enCours = "Sections";
                //on vide le truc et on remercies
                //on vide le dom des questions
                $("#boutonSuivant").remove();
                $("#titre").html('Fin du questionnaire');
                $("#questions").empty().append($("<h4>Merci d'avoir participé et d'avoir répondu à ce questionnaire</h4>"));
                $("#questions").append($(`
                    <h4>Voulez vous voir la représentation de ce questionnaire avec RIVER?</h4>
                    <a id="representation" class="btn btn-danger" name="afficherReprésentation" href="${questionnaire.representation}">Voir la représentation</a>
                `));
            }
            break;
        case "Sections":
            //on vide le truc et on remercies
            //on vide le dom des questions
            $("#boutonSuivant").remove();
            $("#titre").html('Fin du questionnaire');
            $("#questions").empty().append($("<h4>Merci d'avoir participé et d'avoir répondu à ce questionnaire</h4>"));
            $("#questions").append($(`
                <h4>Voulez vous voir la représentation de ce questionnaire avec RIVER?</h4>
                <a id="representation" class="btn btn-danger" name="afficherReprésentation" href="${questionnaire.representation}">Voir la représentation</a>
            `));
            break;
    }
};

export default {
    init : questionnaire.init,
    charger : questionnaire.charger,
    id : questionnaire.id,
    infosSupplementaires : questionnaire.infosSupplementaires,
}