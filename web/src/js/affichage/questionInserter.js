
let questionInserter = {};

//pour les comparaisons
questionInserter.insererQuestionComparaison = function($elemQuestion, data){
    //on créé la table
    let $table = $("<form><table class=\"table\"><thead><tr></tr></thead><tbody class=\"text-center\"></tbody></table></form>");
    $elemQuestion.append($table);

    //on ratache le tout
    $('#questions').append($elemQuestion);

    //on va insérer les réponses possibles
    let $lieuInserstion = $("#q"+data.question.idQuestion + "> form > table > thead > tr");
    $lieuInserstion.append($('<th scope="col"></th>'));

    //on parcout les réponses
    data.reponsesPossibles.forEach(rep => {
        //pour insérer les propositions
        questionInserter.insererReponsesPossiblesComparaison($lieuInserstion, rep)
    });

    //on sélectionne le lieu
    $lieuInserstion = $("#q"+data.question.idQuestion + " > form > table > tbody");
    //on insère maintenant les méthodes
    data.methodes.forEach((meth, index) => {
        questionInserter.insererMethode($lieuInserstion, meth, index, data.reponsesPossibles.length, data.reponsesPossibles);
    })
};

//pour une case à cocher
questionInserter.insererQuestionCase = function($elemQuestion, data, type){
    //on créé la liste
    let $liste = $("<ul></ul>");
    $elemQuestion.append($liste);

    //on va ajouter à la liste tous les éléments
    data.reponsesPossibles.forEach(elem => {
        //on créé les 3 éléments
        let $li = $('<li></li>');
        let $label = $('<label><input value="' + elem.intitule + '" class="" type="radio" name="'+ type + '">' + ' ' + elem.intitule + '</label>');

        //on attache tout
        $li.append($label);
        $liste.append($li);
    });

    //on ajoute en cas de mauvaise reponse
    $elemQuestion.children("p").append($("<span class=\"oi oi-circle-x\" style='display: none;'></span>"));

    //on met dans le formulaire
    let $form = $("<form class='col-md-12'></form>");
    $form.append($elemQuestion);
    //on ratache le tout
    $('#questions').append($form);
};

//pour insérer les méthodes
questionInserter.insererMethode = function ($lieu, methode, index, nbBoutons, reponses) {
    //on définit le fond de la table
    if(index%2===0) var fond = "fond1";
    else fond = "fond2";

    //on créé le noeud
    let $noeud = $('<tr></tr>');
    //avec le bon fond
    $noeud.addClass(fond);

    let $noeudTitre = $('<th scope="row"></th>');
    $noeudTitre.html(methode.nomMethode);
    //on accroche les deux
    $noeud.append($noeudTitre);

    //on rajoute les boutons
    for(var i=0; i<nbBoutons; i++){
        let valeur = reponses[i];
        $noeud.append($('<td><input type="radio" name="' + methode.nomMethode + '" value="' + valeur.intitule + '"></td>'));
    }

    //pour quand c'est incorrect
    $noeud.append($('<td class="message"><span class="oi oi-circle-x"></span></td>'));

    //on rattache
    $lieu.append($noeud);
};

//pour insérer toutes les réponses possibles
questionInserter.insererReponsesPossiblesComparaison = function ($lieu, rep) {
    //on créé le noeud
    let $noeud = $('<th scope="col" class="text-center"></th>');
    $noeud.html(rep.intitule);

    //on accroche
    $lieu.append($noeud);
};

//pour les contact
/*questionInserter.insererQuestionContact = function($elemQuestion){
    //on ajoute le champ du formulaire
    let $champ = $('<input type="text" class="form-control">');
    $elemQuestion.append($champ);

    //on ratache le tout
    $('#questions').append($elemQuestion);
};*/

//pour les mails
/*questionInserter.insererQuestionMail = function ($elemQuestion) {
    //on ajoute le champ du formulaire
    let $champ = $('<input type="email" class="form-control"><div class="invalid-feedback">\n' +
        '          Please choose a username.\n' +
        '        </div>');
    $elemQuestion.append($champ);

    //on ratache le tout
    $('#questions').append($elemQuestion);
};*/

//pour demande si on veut un entretien/les résultats
questionInserter.insererBoolean = function (question, nomTitre) {
    $("#titre").html(nomTitre);

    //on créé la div
    let $elemQuestion = $("<div class='col-md-12'></div>")
        .attr("id", "q" + 1);

    //on créé le titre
    let $titre = $("<p></p>").html(question);
    $elemQuestion.append($titre);

    //les données
    var data = {};
    data.reponsesPossibles = [{intitule : "Oui"}, {intitule : "Non"}];

    //on insère la question
    questionInserter.insererQuestionCase($elemQuestion, data, "boolean");


    //on ratache le tout
    $('#questions').append($elemQuestion);
};

questionInserter.insererSectionContact = function(){
    $("#titre").html("Contact");

    //on vide le dom des questions
    $("#questions").empty();

    var tab = [{intitule : "Nom", type : "text"},
               {intitule : "Prénom", type : "text"},
               {intitule : "Email", type : "email"}];

    tab.forEach(function (elem) {
        //on créé la div
        let $elemQuestion = $("<div class='col-md-12'></div>");

        //on créé le titre
        let $titre = $("<p></p>").html(elem.intitule);
        $elemQuestion.append($titre);

        //le champ
        let $champ = $(`<input type="${elem.type}" class="form-control">`).attr("id", elem.intitule);
        $elemQuestion.append($champ);

        //on ratache le tout
        $('#questions').append($elemQuestion);
    });


};

export default {
    insererMethode : questionInserter.insererMethode,
    insererReponsesPossiblesComparaison : questionInserter.insererReponsesPossiblesComparaison,
    insererQuestionComparaison : questionInserter.insererQuestionComparaison,
    insererQuestionCase : questionInserter.insererQuestionCase,
    insererQuestionContact : questionInserter.insererQuestionContact,
    insererQuestionMail : questionInserter.insererQuestionMail,
    insererBoolean : questionInserter.insererBoolean,
    insererSectionContact : questionInserter.insererSectionContact
}