let radioButton = {};

//pour ajouter un listener si on clique un peu à côté du bouton
radioButton.init = function () {
    //on ajoute le listener sur les cellules
    $("body").on("click", "td",radioButton.clique);
};

radioButton.clique = function (event) {
    var $autour = $(event.target);
    //on cherche le radiobutton et on simule le click
    $autour.children("input").click();
};

export default {
    init : radioButton.init
}