let verifier = {};

verifier.verifier = function () {
    var pasRemplis = verifier.radioComparaison();
    pasRemplis += verifier.radioListe();
    pasRemplis += verifier.champs();

    //pour le message
    if(pasRemplis !==0){
        $("#messageErreur").show();
    }else{
        $("#messageErreur").hide();
    }

    return pasRemplis;
};

verifier.radioComparaison = function () {
    //Radios buttons
    let nbPasRemplis = $('tbody > tr:not(:has(:radio:checked))').length;
    //on vérifie que tous les tableaux ont un de leur radio buttons de cochés
    if (nbPasRemplis) {
        //pour ceux qui n'ont rien de coché
        $('tbody > tr:not(:has(:radio:checked))').children(".message").show();

    }
    //pour ceux qui n'ont plus besoin d'être coché
    $('tbody > tr:has(:has(:radio:checked))').children(".message").hide();

    return nbPasRemplis;
};

verifier.radioListe = function () {
    let nbPasRemplis = $('ul:not(:has(:radio:checked))').length;
    //on vérifie que tous les choix multiples ont un radio de coché
    if (nbPasRemplis) {
        //pour ceux qui n'ont rien de coché
        $('ul:not(:has(:radio:checked))').siblings("p").children("span").show();
    }
    //pour ceux qui n'ont plus besoin d'être coché
    $('ul:has(:has(:radio:checked))').siblings("p").children("span").hide();

    return nbPasRemplis;
};

verifier.champs = function () {
    let nbPasRemplis = 0;
    //on vérifie que tous les champs de formulaire ont été remplis
    $('input[type="text"],[type="email"]').each(function (index, $elem) {
        if($(this).val() === ""){
            $(this).addClass("is-invalid");
            nbPasRemplis++;
        }else{
            $(this).removeClass("is-invalid")
        }
    });

    return nbPasRemplis;
};


export default {
    verifier : verifier.verifier,
}