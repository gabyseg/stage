import loader from "./requetes/loader.js";
import questionnaire from "./questionnaire.js";
import radios from "./affichage/radioButton.js";
import section from "./section.js";
import sender from "./requetes/sender.js";

$(document).ready(function () {
    //on initialise le loader
    loader.init(window.location.origin);
    sender.init(window.location.origin);

    //on initialise le questionnaire
    var q = questionnaire.init();
    //on le charge
    q.then(questionnaire.charger);
    //on initialise les radiosBoutons
    q.then(radios.init());

    //on initialise le bouton
    q.then(function () {
        $('#boutonSuivant').on("click", section.suivante);
    });
});