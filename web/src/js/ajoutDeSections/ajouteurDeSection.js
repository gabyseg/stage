import sender from "../requetes/sender.js";
import loader from "../requetes/loader.js";

function nouvelleSection(event) {
    //on regarde si c'est un nouveau ou une réutilisation
    if($("input[type=radio][name='section']:checked").val() === "nouvSection"){
        //on sélectionne le champ de la nouvelle section
        let text = $("#nouvelleSection").val();

        //on envoie la requete au serveur
        sender.envoyer("/api/creation/nouvelleSection", {"intituleSection" :text, "mode" : "nouveau"}).then(function (reponse) {
            console.log(reponse);
            //puis on insère la nouvelle section dans le dom
            let $li = $(`<li><a href="/river/creation/section/${reponse.data}">${text} <span class="oi oi-pencil boutonModif"></span> </a><span class="oi oi-delete float-right"></span></li>`);
            //on ajoute le listener de supression
            $li.find(".oi-delete").click(supprimerNouvelleSection);
            $("#sections").append($li);

            //on ajoute en attribut
            $li.attr("id", "s" + reponse.data);
        });

        //on remet à 0 la fenêtre
        $("#nouvelleSection").val("");
    }else{
        //on vérifie qu'au moins un champ est sélectionné
        if($("#listeSection button.active").length ===1){
            let id = $("#listeSection button.active").attr("id").replace("sect", "");
            let text = $("#listeSection button.active").html();

            //on envoie
            sender.envoyer("/api/creation/nouvelleSection", {"id" :id, "mode": "ancien"}).then(function (reponse) {
                console.log(reponse);
                //puis on insère la nouvelle section dans le dom
                let $li = $(`<li><a href="/river/creation/section/${id}">${text} <span class="oi oi-pencil boutonModif"></span> </a><span class="oi oi-delete float-right"></span></li>`);
                //on ajoute le listener de supression
                $li.find(".oi-delete").click(supprimerNouvelleSection);
                $("#sections").append($li);

                //on ajoute en attribut
                $li.attr("id", "s" + id);
            });

        }else{
            event.stopPropagation();
        }
    }
}

function nouvelElement(event) {
    //on vérifie le nombre
    if($("#elements").children().length < 4){
        //on regarde si c'est un nouveau ou une réutilisation
        if($("input[type=radio][name='element']:checked").val() === "nouvElement"){
            //on sélectionne le champ de la nouvelle section
            let text = $("#nouvelElement").val();

            //on vérifie qu'il y a qqch
            if(text !== ""){
                //on envoie la requete au serveur
                sender.envoyer("/api/creation/nouvelElement", {"intituleElement" :text, "mode" : "nouveau"}).then(function (reponse) {
                    console.log(reponse);
                    //puis on insère la nouvelle section dans le dom
                    let $li = $(`<li>${text} <span class="oi oi-delete boutonSupression"></span></li>`);
                    //on ajoute le listener de supression
                    $li.click(supprimerNouvelElement);
                    $("#elements").append($li);

                    //on ajoute en attribut
                    $li.attr("id", "e" + reponse.data);
                });

                //on remet à 0 la fenêtre
                $("#nouvelElement").val("");
            }else{
                event.stopPropagation();
            }
        }else{
            //on vérifie qu'au moins un champ est sélectionné
            if($("#listeElem button.active").length ===1){
                let id = $("#listeElem button.active").attr("id").replace("elem", "");
                let text = $("#listeElem button.active").html();

                //on envoie
                sender.envoyer("/api/creation/nouvelElement", {"id" :id, "mode": "ancien"}).then(function (reponse) {
                    console.log(reponse);
                    //puis on insère la nouvelle section dans le dom
                    let $li = $(`<li>${text} <span class="oi oi-delete boutonSupression"></span></li>`);
                    //on ajoute le listener de supression
                    $li.click(supprimerNouvelElement);
                    $("#elements").append($li);

                    //on ajoute en attribut
                    $li.attr("id", "e" + id);
                });

            }else{
                event.stopPropagation();
            }
        }
    }else{
        alert("La technique est prévue pour fonctionner au maximum avec 4 éléments");
    }


}

function supprimerNouvelleSection(event) {
    let $li = $(event.currentTarget.parentElement);
    // var $enlev = $li.find("span").remove();

    //on envoie la requête
    sender.supprimer("/api/supprimer/section/" + $li.attr("id").replace("s", "")).then(function (reponse) {
        //une fois qu'on a la réponse on enlève la puce
        $li.remove();
        console.log(reponse);
    });
}

function supprimerNouvelElement(event) {
    let $li = $(event.currentTarget);
    // var $enlev = $li.find("span").remove();

    //on envoie la requête
    sender.supprimer("/api/supprimer/element/" + $li.attr("id").replace("e", "")).then(function (reponse) {
        //une fois qu'on a la réponse on enlève la puce
        $li.remove();
        console.log(reponse);
    });
}

function switchTypeElement(event) {
    //on vide la section element
    let $div = $("#elem");
    $div.empty();

    //selon ce qu'on veut
    if($(event.target).val() === "nouvElement"){
        //on affiche le code html correspondant
        $div.append('<label for="nouvelElement">Nouvel élément de comparaison</label>\n' +
            '    <input type="text" class="form-control" id="nouvelElement" placeholder="Nom du nouvel élément">');
    }else {
        //html correspondant
        $div.append('<form class="form-inline">\n' +
            '  <input id="elementChercher" class="form-control form-control-sm col-md" type="text" placeholder="Chercher" aria-label="Chercher">\n' +
            '<span class="oi oi-magnifying-glass pl-3 pr-3" id="chercherElement"></span>' +
            '</form>');

        //la liste des éléments
        let $elem = $('<div class="list-group col-md-11 liste" id="listeElem"></div>');
        loader.init("http://localhost:8888");

        $div.append($elem);

        //on remplit ici les éléments
        loader.charger("/api/elementsComparaison/get").then(function (reponse) {
            let elements = reponse.data;
            elements.forEach(function(elem){
                //on construit l'élément
                let $temp = $(`  <button type="button" id="elem${elem.idMethode}" class="list-group-item list-group-item-action">${elem.nomMethode}</button>`);
                $temp.click(selecteurDAncien);
                $elem.append($temp);
            });
        });

        $("#chercherElement").click(chercherElement);
    }
}

function switchTypeSection(event) {
    //on vide la section element
    let $div = $("#sectio");
    $div.empty();

    //selon ce qu'on veut
    if($(event.target).val() === "nouvSection"){
        //on affiche le code html correspondant
        $div.append('<label for="nouvelleSection">Nouvelle section</label>'
            +'<input type="text" class="form-control" id="nouvelleSection" placeholder="Nom de la nouvelle section">');
    }else {
        //html correspondant
        $div.append('<form class="form-inline">\n' +
            '  <input class="form-control form-control-sm col-md" type="text" placeholder="Chercher" aria-label="Chercher" id="textChercher">\n' +
            '<span id="chercherSection" class="oi oi-magnifying-glass pl-3 pr-3"></span>' +
            '</form>');

        //la liste des éléments
        let $elem = $('<div class="list-group col-md-11 liste" id="listeSection"></div>');
        loader.init("http://localhost:8888");

        $div.append($elem);

        //on remplit ici les éléments
        loader.charger("/api/sections/get").then(function (reponse) {
            console.log(reponse);
            let elements = reponse.data;
            elements.forEach(function(section){
                //on construit l'élément
                let $temp = $(`  <button type="button" id="sect${section.idSection}" class="list-group-item list-group-item-action">${section.intitule}</button>`);
                $temp.click(selecteurDAncienneSection);
                $elem.append($temp);
            });
        });

        //pour la recherche de section
        $("#chercherSection").on("click", chercherSection);
    }
}

function chercherSection(){
    //on vérifie qu'on cherche qqch
    var url = "";
    if($("#textChercher").val() !== ""){
        url = "/api/sections/get/"+ $("#textChercher").val();
    }else{
        url = "/api/sections/get";
    }

    //on remplit ici les éléments
    loader.charger(url).then(function (reponse) {
        $("#listeSection").empty();
        let $elem = $("#listeSection");
        console.log(reponse);
        let elements = reponse.data;
        elements.forEach(function(section){
            //on construit l'élément
            let $temp = $(`  <button type="button" id="sect${section.idSection}" class="list-group-item list-group-item-action">${section.intitule}</button>`);
            $temp.click(selecteurDAncienneSection);
            $elem.append($temp);
        });
    });
}

function chercherElement(){
    //on vérifie qu'on cherche qqch
    var url = "";
    if($("#elementChercher").val() !== ""){
        url = "/api/elementsComparaison/get/"+ $("#elementChercher").val();
    }else{
        url = "/api/elementsComparaison/get";
    }

    //on remplit ici les éléments
    loader.charger(url).then(function (reponse) {
        $("#listeElem").empty();
        let $elem = $("#listeElem");
        console.log(reponse);
        let elements = reponse.data;
        elements.forEach(function(met){
            //on construit l'élément
            let $temp = $(`  <button type="button" id="elem${met.idMethode}" class="list-group-item list-group-item-action">${met.nomMethode}</button>`);
            $temp.click(selecteurDAncien);
            $elem.append($temp);
        });
    });
}

function selecteurDAncien(event){
    //on sélectionne le bouton
    $("#listeElem button").removeClass("active");
    $(event.target).addClass("active");
}

function selecteurDAncienneSection(event){
    //on sélectionne le bouton
    $("#listeSection button").removeClass("active");
    $(event.target).addClass("active");
}

export default {
    nouvelleSection : nouvelleSection,
    nouvelElement : nouvelElement,
    supprimerNouvelElement : supprimerNouvelElement,
    supprimerNouvelleSection : supprimerNouvelleSection,
    switchTypeElement : switchTypeElement,
    switchTypeSection : switchTypeSection,
    chercherSection : chercherSection,
}