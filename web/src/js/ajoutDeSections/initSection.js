import ajouteur from "./ajouteurDeSection.js";
import sender from "../requetes/sender.js";
import ajouteurContact from "./ajouteurContact.js";
import ajouteurDeReponses from "./ajouteurReponses.js";
//import loader from "../requetes/loader.js";

$(document).ready(function () {
    //on initialise le sender
    sender.init(window.location.origin);
    //loader.init("http://localhost:8888");

    //pour le bouton d'ajout de question
    $("#boutonAjoutQuestion").css("cursor", "pointer");

    //pour le bouton pour ajouter une réponse
    $("#ajouterReponse").click(ajouteurDeReponses.ajouterChamp);

    //pour quand on ajoute une nouvelle réponse
    $("#ajouterQuestion").on("click", ajouteurDeReponses.sauvegarderNouvelleReponse);

    //pour supprimer une question
    $("span.delete").click(ajouteurDeReponses.supprimerQuestion);

    //pour quand on annule un ajout
    $("#annuler").on("click", ajouteurDeReponses.reinitModale);
    $("#close").on("click", ajouteurDeReponses.reinitModale);

    //pour quand on veut reutiliser une question
    $("input[type=radio][name='question']").on("change",ajouteurDeReponses.switchTypeQuestion);
});