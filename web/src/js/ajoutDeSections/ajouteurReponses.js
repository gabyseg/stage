import sender from "../requetes/sender.js";
import loader from "../requetes/loader.js";

function ajouterChamp(event) {
    //on sélectionne le bouton
    let $bouton = $(event.target);

    //on créé le nouvel élément
    let $div = $('<div class="form-group col-md" id="formAjoutRep"></div>');
    let $div2 = $("<div class='row'></div>");
    let $input = $('<input class="form-control col-lg" type="text" id="nouvelleReponse" placeholder="Nom de la nouvelle reponse"/>');

    //on rattache le tout
    $div2.append($input);
    $div.append($div2);
    $div.insertBefore($bouton);

    //on change le texte du bouton
    $bouton.html("L'ajouter");
    //on désactive l'ajout du champ
    $bouton.unbind("click", ajouterChamp);
    //on ajoute l'autre fonction
    $bouton.click(ajouter);
}

function ajouter(event) {
    //on sélectionne le champ
    let $champ = $("#nouvelleReponse");
    //on récupère sa valeur
    let val = $champ.val();

    //si il ya bien une valeur
    if(val !== "") {
        //on enlève la partie du champ
        $("#formAjoutRep").remove();

        //on sélectionne la liste
        let $liste = $("#listeReponses");

        //on fabrique la réponse
        let $rep = $("<p></p>");
        $rep.html("" + val);

        //on attache
        $liste.append($rep);

        //on remet le listener
        $(event.target).unbind("click", ajouter);
        $(event.target).on("click", ajouterChamp);
        $(event.target).html("Ajouter une réponse");
    }
}

function sauvegarderNouvelleReponse(event) {
    //on regarde le mode d'ajout
    console.log($("input[type=radio][name='question']:checked").val());
    if($("input[type=radio][name='question']:checked").val() === "nouvelleQuestion"){
        //on vérifie que les champs sont biens
        if(verifier()){
            //on récupère les infos
            //la question
            let question = $("#nomNouvelleQuestion").val();

            //on récupère les réponses
            let tabReponse = [];
            $("#listeReponses p").each(function (index, elem) {
                tabReponse.push($(elem).html());
            });

            let variable = $("#variable").val();

            //on construit les données
            let donnees = {"question" : question, "reponses" : tabReponse, variable : variable, "mode" : "nouveau"};

            //on envoie la requête
            sender.envoyer("/api/creation/nouvelleQuestion", donnees).then(function (reponse) {
                console.log(reponse);

                //quand c'est bon on vide les champs
                $("#nomNouvelleQuestion").val("");
                $("#variable").val("");

                //on remet le listener
                $("#ajouterReponse").unbind("click", ajouter);
                $("#ajouterReponse").unbind("click", ajouterChamp);
                $("#ajouterReponse").on("click", ajouterChamp);
                $("#ajouterReponse").html("Ajouter une réponse");

                //on enlève le champ
                $("#formAjoutRep").remove();

                //on enlève les réponses
                $("#listeReponses").empty();

                //enfin on ajoute dans le dom
                let $div = $("<div></div>");
                //on créé la partie pour la question
                let $q = $(`<div class="col-md" id="q${reponse.data}"><h5>${question} : ${variable}<span class="oi oi-delete float-right delete"></span></h5></div>`);
                $div.append($q);
                //le listener
                $q.find("span").on("click", supprimerQuestion);

                //la liste des éléments de réponse
                let $div2 = $("<div class='col-lg ml-5'></div>");
                let $liste = $("<ul class='listeDecoree'></ul>");

                //pour chaque elemen de reponse
                tabReponse.forEach(function (elem) {
                    let $temp = $("<li></li>").html(elem);
                    $liste.append($temp)
                });

                //on raccroche la liste à la div
                $div2.append($liste);
                $div.append($div2);

                //on attache
                $("#questions").append($div);
            });
        }else{
            event.stopPropagation();
        }
    }else{
        //on vérifie qu'au moins un champ est sélectionné
        if($("#listeQuest button.active").length ===1){
            let id = $("#listeQuest button.active").attr("id").replace("q", "");
            let text = $("#listeQuest button.active").html();

            //on envoie
            sender.envoyer("/api/creation/nouvelleQuestion", {"id" :id, "mode": "ancien"}).then(function (reponse) {
                console.log(reponse);
                let data = reponse.data;


                //enfin on ajoute dans le dom
                let $div = $("<div></div>");
                //on créé la partie pour la question
                let $q = $(`<div class="col-md" id="q${id}"><h5>${text} : ${data.variable.nom}<span class="oi oi-delete float-right delete"></span></h5></div>`);
                $div.append($q);
                //le listener
                $q.find("span").on("click", supprimerQuestion);

                //la liste des éléments de réponse
                let $div2 = $("<div class='col-lg ml-5'></div>");
                let $liste = $("<ul class='listeDecoree'></ul>");

                //pour chaque elemen de reponse
                reponse.data.reponses.forEach(function (elem) {
                    let $temp = $("<li></li>").html(elem.intitule);
                    $liste.append($temp);
                });

                //on raccroche la liste à la div
                $div2.append($liste);
                $div.append($div2);

                //on attache
                $("#questions").append($div);
            });

        }else{
            event.stopPropagation();
        }
    }



}

//pour supprimer une question
function supprimerQuestion(event) {
    //on récupère la div
    let $div = $(event.target).parent().parent();
    //on récupère son id
    let id = $div.attr("id");
    id = id.replace("q", "");

    //on envoie la requête
    sender.supprimer("/api/supprimer/question/"+id).then(function (reponse) {
        console.log(reponse);

        //on enlève dans le dom
        $div.parent().remove();
    });
}

//pour tout enlever
function reinitModale(event) {
    //quand c'est bon on vide les champs
    $("#nomNouvelleQuestion").val("");
    //on enlève le champ
    $("#formAjoutRep").remove();
    //on enlève les réponses
    $("#listeReponses").empty();
    //la variable
    $("#variable").val("");

    //le bouton
    $("#ajouterReponse").unbind("click", ajouter);
    $("#ajouterReponse").unbind("click", ajouterChamp);

    $("#ajouterReponse").on("click", ajouterChamp);
    $("#ajouterReponse").html("Ajouter une réponse");
}

//pour vérifier qu'il y a au moins une question et une réponse
function verifier() {
    if($("#nomNouvelleQuestion").val() !== "" && $("#listeReponses").children().length !== 0){
        //c'est bon
        return true;
    }else{
        return false;
    }
}

function switchTypeQuestion(event) {
    //on vide la section element
    let $div = $("#quest");
    $div.empty();

    //selon ce qu'on veut
    if($(event.target).val() === "nouvelleQuestion"){
        //on affiche le code html correspondant
        $div.append('<div class="form-group">\n' +
            '                                <label for="nouvelElement">Nouvelle question</label>\n' +
            '                                <textarea class="form-control" id="nomNouvelleQuestion" rows="3" placeholder="Nouvelle question"></textarea>\n' +
            '                            </div>\n' +
            '                            \n' +
            '                            <hr>\n' +
            '                            \n' +
            '                            <div class="form-group">\n' +
            '                                <label for="variable">Variable :</label>\n' +
            '                                <input class="form-control" id="variable" type="text" placeholder="Nom de la variable" />\n' +
            '                            </div>\n' +
            '                            \n' +
            '                            <hr>\n' +
            '                            <div id="reponses">\n' +
            '                                <p>Réponses possibles</p>\n' +
            '                                <div id="listeReponses">\n' +
            '                                \n' +
            '                                </div>\n' +
            '                                <button class="btn btn-primary btn-sm" id="ajouterReponse">Ajouter une réponse</button>\n' +
            '                            </div>');

        //on rajoute le listener
        $div.find("button").on("click", ajouterChamp);
    }else {
        //html correspondant
        $div.append('<form class="form-inline">\n' +
            '  <input id="textQuestion" class="form-control form-control-sm col-md" type="text" placeholder="Chercher" aria-label="Chercher">\n' +
            '<span id="chercherQuest" class="oi oi-magnifying-glass pl-3 pr-3"></span>' +
            '</form>');

        //la liste des éléments
        let $elem = $('<div class="list-group col-md-11 liste" id="listeQuest"></div>');

        loader.init("http://localhost:8888");

        $div.append($elem);

        //on remplit ici les éléments
        loader.charger("/api/questions/get").then(function (reponse) {
            let elements = reponse.data;
            elements.forEach(function(elem){
                console.log(elem);
                //on construit l'élément
                let $temp = $(`  <button type="button" id="q${elem.idQuestion}" class="list-group-item list-group-item-action">${elem.intitule}</button>`);
                $temp.click(selecteurDAncien);
                $elem.append($temp);
            });
        });


        $("#chercherQuest").click(chercherQuestion);
    }


}

function selecteurDAncien(event){
    //on sélectionne le bouton
    $("#listeQuest button").removeClass("active");
    $(event.target).addClass("active");
}

function chercherQuestion(){
    //on vérifie qu'on cherche qqch
    var url = "";
    if($("#textQuestion").val() !== ""){
        url = "/api/questions/get/"+ $("#textQuestion").val();
    }else{
        url = "/api/questions/get";
    }

    //on remplit ici les éléments
    loader.charger(url).then(function (reponse) {
        $("#listeQuest").empty();
        let $elem = $("#listeQuest");
        console.log(reponse);
        let elements = reponse.data;
        elements.forEach(function(q){
            //on construit l'élément
            let $temp = $(`  <button type="button" id="q${q.idQuestion}" class="list-group-item list-group-item-action">${q.intitule}</button>`);
            $temp.click(selecteurDAncien);
            $elem.append($temp);
        });
    });
}

export default {
    ajouterChamp: ajouterChamp,
    sauvegarderNouvelleReponse : sauvegarderNouvelleReponse,
    supprimerQuestion : supprimerQuestion,
    reinitModale : reinitModale,
    verifier : verifier,
    switchTypeQuestion : switchTypeQuestion
}