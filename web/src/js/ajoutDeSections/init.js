import ajouteur from "./ajouteurDeSection.js";
import sender from "../requetes/sender.js";
import ajouteurContact from "./ajouteurContact.js";

$(document).ready(function () {
    //on initialise le sender
    sender.init(window.location.origin);

    //pour changer le curseur quand on veut ajout un truc
    $("#boutonAjoutSection").css("cursor", "pointer");
    $("#boutonAjoutComparaison").css("cursor", "pointer");

    //on initialise le sender
    sender.init(window.location.origin);

    //on initialise le bouton ajouter
    $("#ajouterSection").click(ajouteur.nouvelleSection);
    $("#ajouterElement").click(ajouteur.nouvelElement);

    //on réinitialise les liste et leur clic
    $("#sections li .oi-delete").click(ajouteur.supprimerNouvelleSection);
    $("#elements li").click(ajouteur.supprimerNouvelElement);

    //pour les radio buttons
    $("input[type=radio][name='resultats']").click(ajouteurContact.ajouterInfoDeContact);
    $("input[type=radio][name='entretien']").click(ajouteurContact.ajouterInfoDeContact);

    //pour les anciennes sections
    $("input[type=radio][name='element']").on("change",ajouteur.switchTypeElement);
    $("input[type=radio][name='section']").on("change",ajouteur.switchTypeSection);
});