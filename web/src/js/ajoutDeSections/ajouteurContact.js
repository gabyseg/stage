import sender from "../requetes/sender.js";
function ajouterInfoDeContact(event) {
    let donnes = {"name" : $(event.target).attr("name"), "valeur" : $(event.target).val()};

    //on envoie les données
    sender.envoyer("/api/creation/infoContact", donnes).then(function (reponse) {
        console.log(reponse);
    });
}

export default {
    ajouterInfoDeContact : ajouterInfoDeContact
}