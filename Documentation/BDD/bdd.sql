-- Pour supprimer les clés étrangères
ALTER TABLE ReponsePossible DROP FOREIGN KEY fk_type;
ALTER TABLE SousReponse DROP FOREIGN KEY fk_methode;
ALTER TABLE Reponse DROP FOREIGN KEY fk_questionnaire;
ALTER TABLE Reponse DROP FOREIGN KEY fk_question;
ALTER TABLE Reponse DROP FOREIGN KEY fk_repPossible;
ALTER TABLE Reponse DROP FOREIGN KEY fk_personne;
ALTER TABLE Personne DROP FOREIGN KEY fk_contact;
ALTER TABLE Personne DROP FOREIGN KEY fk_profession;
ALTER TABLE Personne DROP FOREIGN KEY fk_specialite;
ALTER TABLE Possede DROP FOREIGN KEY fk_questionnaire2;
ALTER TABLE Possede DROP FOREIGN KEY fk_question2;
ALTER TABLE Concerne DROP FOREIGN KEY fk_repPos;
ALTER TABLE Concerne DROP FOREIGN KEY fk_rep;
ALTER TABLE Concerne DROP FOREIGN KEY fk_met;



-- Création des tables --
DROP TABLE IF EXISTS Questionnaire;
CREATE TABLE Questionnaire(
    idQuestionnaire int AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(100)
);

DROP TABLE IF EXISTS Section;
CREATE TABLE Section (
    idSection int AUTO_INCREMENT PRIMARY KEY,
    intitule text
);

DROP TABLE IF EXISTS Question;
CREATE TABLE Question(
    idQuestion int AUTO_INCREMENT PRIMARY KEY,
    intitule text,
    idSection int
);

DROP TABLE IF EXISTS ReponsePossible;
CREATE TABLE ReponsePossible(
    idReponse int AUTO_INCREMENT PRIMARY KEY,
    intitule VARCHAR(100),
    idType int
);

DROP TABLE IF EXISTS TypeReponse;
CREATE TABLE TypeReponse(
    idType int AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(100)
);

DROP TABLE IF EXISTS SousReponse;
CREATE TABLE SousReponse(
    idSousReponse int AUTO_INCREMENT PRIMARY KEY,
    intitule VARCHAR(100),
    idMethode int
);

DROP TABLE IF EXISTS Methode;
CREATE TABLE Methode(
    idMethode int AUTO_INCREMENT PRIMARY KEY,
    nomMethode VARCHAR(50)
);

DROP TABLE IF EXISTS Reponse;
CREATE TABLE Reponse(
    idReponse int AUTO_INCREMENT PRIMARY KEY,
    idQuestionnaire int,
    idQuestion int,
    idReponsePossible int,
    idPersonne int
);

DROP TABLE IF EXISTS Contact;
CREATE TABLE Contact(
    idContact int AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    mail VARCHAR(100)
);

DROP TABLE IF EXISTS Personne;
CREATE TABLE Personne(
    idPersonne int AUTO_INCREMENT PRIMARY KEY,
    resultat boolean,
    entretien boolean,
    idContact int,
    idProfession int,
    idSpecialite int
);

DROP TABLE IF EXISTS Profession;
CREATE TABLE Profession(
    idProfession int AUTO_INCREMENT PRIMARY KEY,
    intitule VARCHAR(100)
);

DROP TABLE IF EXISTS Specialite;
CREATE TABLE Specialite(
    idSpecialite int AUTO_INCREMENT PRIMARY KEY,
    intitule VARCHAR(100)
);

DROP TABLE IF EXISTS Possede;
CREATE TABLE Possede(
    idQuestionnaire int,
    idQuestion int,
    PRIMARY KEY (idQuestionnaire, idQuestion)
);

DROP TABLE IF EXISTS APourSection;
CREATE TABLE APourSection(
    idQuestionnaire int,
    idSection int,
    PRIMARY KEY (idQuestionnaire, idSection)
);

DROP TABLE IF EXISTS EstPossible;
CREATE TABLE EstPossible(
    idQuestion int,
    idReponsePossible int,
    PRIMARY KEY (idQuestion, idReponsePossible)
);

DROP TABLE IF EXISTS APourSousReponse;
CREATE TABLE APourSousReponse(
    idReponse int,
    idSousReponse int,
    PRIMARY KEY (idReponse, idSousReponse)
);

DROP TABLE IF EXISTS Concerne;
CREATE TABLE Concerne(
  id int AUTO_INCREMENT PRIMARY KEY ,
  idReponse int,
  idReponsePossible int,
  idMethode int
);

-- Clés étrangères --
ALTER TABLE ReponsePossible ADD CONSTRAINT fk_type FOREIGN KEY (idType) REFERENCES TypeReponse(idType);

ALTER TABLE SousReponse ADD CONSTRAINT fk_methode FOREIGN KEY (idMethode) REFERENCES Methode (idMethode);

ALTER TABLE Reponse ADD CONSTRAINT fk_questionnaire FOREIGN KEY (idQuestionnaire) REFERENCES Questionnaire(idQuestionnaire);
ALTER TABLE Reponse ADD CONSTRAINT fk_question FOREIGN KEY (idQuestion) REFERENCES Question(idQuestion);
ALTER TABLE Reponse ADD CONSTRAINT fk_repPossible FOREIGN KEY (idReponsePossible) REFERENCES ReponsePossible(idReponse);
ALTER TABLE Reponse ADD CONSTRAINT fk_personne FOREIGN KEY (idPersonne) REFERENCES Personne(idPersonne);

ALTER TABLE Personne ADD CONSTRAINT fk_contact FOREIGN KEY (idContact) REFERENCES Contact(idContact);
ALTER TABLE Personne ADD CONSTRAINT fk_profession FOREIGN KEY (idProfession) REFERENCES Profession(idProfession);
ALTER TABLE Personne ADD CONSTRAINT fk_specialite FOREIGN KEY (idSpecialite) REFERENCES Specialite(idSpecialite);

ALTER TABLE Possede ADD CONSTRAINT fk_questionnaire2 FOREIGN KEY (idQuestionnaire) REFERENCES Questionnaire(idQuestionnaire);
ALTER TABLE Possede ADD CONSTRAINT fk_question2 FOREIGN KEY (idQuestion) REFERENCES Question(idQuestion);

ALTER TABLE Concerne ADD CONSTRAINT fk_repPos FOREIGN KEY (idReponsePossible) REFERENCES ReponsePossible(idReponse);
ALTER TABLE Concerne ADD CONSTRAINT fk_rep FOREIGN KEY (idReponse) REFERENCES Reponse(idReponse);
ALTER TABLE Concerne ADD CONSTRAINT fk_met FOREIGN KEY (idMethode) REFERENCES Methode(idMethode);


-- Création des données
INSERT INTO Questionnaire (nom) VALUES ('Questionnaire n°1');
INSERT INTO Section VALUES (1, 'Prendre connaissance de ces 3 méthodes et être convaincu');
INSERT INTO Section VALUES (2, 'Se former et disposer de ces méthodes');
INSERT INTO Section VALUES (6, 'Profession');

INSERT INTO APourSection VALUES (1, 1);
INSERT INTO APourSection VALUES (1, 2);
INSERT INTO APourSection VALUES (1, 6);


-- Création des questions
INSERT INTO Question (intitule, idSection) VALUES ('Estimez votre première impression, lors de votre découverte de cette méthode de diagnostic ?', 1);
INSERT INTO Question (intitule, idSection) VALUES ('A votre avis, quelle est la disponibilité de l\'instrument nécessaire au diagnostic ?', 2);
INSERT INTO Question (intitule, idSection) VALUES ('Quelle est votre profession ?', 6);

-- Pour le lien entre questions et questionnaire
INSERT INTO Possede VALUES (1,1);
INSERT INTO Possede VALUES (1,2);
INSERT INTO Possede VALUES (1,3);


-- Création des types de réponses
INSERT INTO TypeReponse (nom) VALUES ('Comparaison : mauvais à bon');
INSERT INTO TypeReponse (nom) VALUES ('Comparaison : indisponible à disponible');
INSERT INTO TypeReponse (nom) VALUES ('Profession');

-- Création des méthodes
INSERT INTO Methode (nomMethode) VALUES ('TEP');
INSERT INTO Methode (nomMethode) VALUES ('IRM');
INSERT INTO Methode (nomMethode) VALUES ('Scanner');

-- Création de professions
INSERT INTO Profession (intitule) VALUES ('Clinicien');
INSERT INTO Profession (intitule) VALUES ('Médecin');
INSERT INTO Profession (intitule) VALUES ('Chercheur');

-- Créations des spécialités
INSERT INTO Specialite (intitule) VALUES ('Neurologie');
INSERT INTO Specialite (intitule) VALUES ('Cardiologie');
INSERT INTO Specialite (intitule) VALUES ('Dermatologie');
INSERT INTO Specialite (intitule) VALUES ('Endocrinologie');
INSERT INTO Specialite (intitule) VALUES ('Gastro-entérologie');
INSERT INTO Specialite (intitule) VALUES ('Gynécologie');
INSERT INTO Specialite (intitule) VALUES ('Hépatologie');
INSERT INTO Specialite (intitule) VALUES ('Néphrologie');
INSERT INTO Specialite (intitule) VALUES ('Oncologie');
INSERT INTO Specialite (intitule) VALUES ('Pneumologie');

-- Création de Réponses possibles
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Très mauvaise', 1);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Plutôt mauvaise', 1);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Moyennement bonne', 1);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Plutôt bonne', 1);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Très bonne', 1);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('NSP', 1);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('NA', 1);

INSERT INTO ReponsePossible (intitule, idType) VALUES ('Indisponible', 2);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Très peu disponible', 2);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Moyennement disponible', 2);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Plutôt disponible', 2);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('Très disponible', 2);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('NSP', 2);
INSERT INTO ReponsePossible (intitule, idType) VALUES ('NA', 2);


